<div class="login_panel hide">
  <div class="login_panel-container">
    <div class="login-panel_box">
      <div class="login_method-tab_box">
        <div class="login_method-tab_btn wx_login">
          <img src="../assets/images/qcode_icon.png" alt="" />
        </div>
        <div class="login_method-tab_btn pc_login">
          <img src="../assets/images/pc_login.png" alt="" />
        </div>
        <div class="login_weixin-tab_btn button">
          <div class="iconfont icon-weixin1"></div>
          <span class="text">微信登录</span>
        </div>
      </div>
    </div>
    <div class="login_QR_code-panel">
      <div class="weixin-QR_box">
        <img src="../assets/images/qrcode_image.png" alt="" />
        <div class="text">使用微信扫描二维码登录</div>
      </div>
      <div class="login_panel-oper_btn flex_yc">
        <div class="login_panel-oper_btn--login">
          <span class="text">账号登录</span>
        </div>
        <div class="divi_line"></div>
        <div class="login_panel-oper_btn--regiest">
          <span class="text">注册新号</span>
        </div>
      </div>
    </div>
    <div class="login_account_panel">
      <div class="login_account_panel-login_panel-wrapper">
        <div class="login_account_panel-login_panel">
          <div id="tabView" class="ui-tab-tabs">
            <i class="ui-tab-line"></i>
              <a href="javascript:" class="ui-tab-tab active" data-rel="account">账号登录</a>
              <a href="javascript:" class="ui-tab-tab" data-rel="sms">短信登录</a>
          </div>
          <div class="ui-tab-contents">
              <div id="account" class="component-tab_card-item ui-tab-content clearfix active">
                <form action="">
                  <div class="username-input login_account_panel-login_panel-form_input clearfix">
                    <div class="input_wrapper">
                      <input type="text" id="username-input" class="ui-input">
                      <label for="username-input" class="ui-input-placeholder">
                        <span class="text">手机号</span>
                        <span class="text err_msg">(请输入正确的手机号)</span>
                      </label>
                    </div>
                  </div>
                  <div class="username-input login_account_panel-login_panel-form_input clearfix">
                    <div class="input_wrapper">
                      <input type="password" id="password-input" class="ui-input">
                      <label for="password-input" class="ui-input-placeholder">
                        <span class="text">密码</span>
                        <span class="text err_msg">(请输入密码)</span>
                      </label>
                    </div>
                  </div>
                  <div class="login_account_panel-login_panel-form_footer">
                    <div class="login_account_panel-login_panel-form_footer-row">
                      <div class="page_component_verify">
                        <span class="text">点击完成验证</span>
                      </div>
                    </div>
                    <div class="login_account_panel-login_panel-form_footer-row">
                      <div class="page_component_login">
                        <span class="text">登录</span>
                      </div>
                    </div>
                    <div class="login_account_panel-login_panel-form_footer-row flex_bc">
                      <div class="pos_left">
                        <input type="checkbox" class="checkbox_autoLogin" id="checkbox_autoLogin" name="checkbox">
                        <label for="checkbox_autoLogin" class="ui-checkbox"></label>
                        <label for="checkbox_autoLogin" class="ui-checkbox-text">下次自动登录</label>
                      </div>
                      <div class="pos_right">
                        <a href="./" target="_blank"><span class="text">忘记密码?</span></a>
                        <a href="./" target="_blank"><span class="text">注册</span></a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div id="sms" class="component-tab_card-item ui-tab-content clearfix">
                <form action="">
                  <div class="username-input login_account_panel-login_panel-form_input clearfix">
                    <div class="input_wrapper">
                      <input type="text" id="sms-username-input" class="ui-input">
                      <label for="sms-username-input" class="ui-input-placeholder">
                        <span class="text">手机号</span>
                        <span class="text err_msg">(请输入正确的手机号)</span>
                      </label>
                    </div>
                  </div>
                  <div class="username-input login_account_panel-login_panel-form_input clearfix">
                    <div class="input_wrapper">
                      <input type="password" id="sms-password-input" class="ui-input">
                      <label for="sms-password-input" class="ui-input-placeholder">
                        <span class="text">验证码</span>
                        <span class="text err_msg">(请输入验证码)</span>
                      </label>
                    </div>
                  </div>
                  <div class="login_account_panel-login_panel-form_footer">
                    <div class="login_account_panel-login_panel-form_footer-row">
                      <div class="page_component_verify">
                        <span class="text">点击完成验证</span>
                      </div>
                    </div>
                    <div class="login_account_panel-login_panel-form_footer-row">
                      <div class="page_component_login">
                        <span class="text">登录</span>
                      </div>
                    </div>
                    <div class="login_account_panel-login_panel-form_footer-row flex_bc">
                      <div class="pos_left">
                        <input type="checkbox" class="checkbox_autoLogin" id="checkbox_autoLogin1" name="checkbox">
                        <label for="checkbox_autoLogin1" class="ui-checkbox"></label>
                        <label for="checkbox_autoLogin1" class="ui-checkbox-text">下次自动登录</label>
                      </div>
                      <div class="pos_right">
                        <a href="./" target="_blank"><span class="text">忘记密码?</span></a>
                        <a href="./" target="_blank"><span class="text">注册</span></a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
