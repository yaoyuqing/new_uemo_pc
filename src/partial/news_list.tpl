<div class="content_list">
  <div class="item_block wow">
    <a href="./news_post.html" class="item_box">
      <div class="item_info clearfix">
        <p class="title fl ellipsis">
          个人、企业网站建设常见问题 Q&A
        </p>
        <div class="date_wrap fr">
          <span class="year">2016</span>
          <i class="time-connect">-</i>
          <span class="m">12</span>
          <i class="time-connect">-</i>
          <span class="d">14</span>
        </div>
      </div>
      <div class="item_des">
        <p>
          根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
        </p>
      </div>
    </a>
  </div>
  <div class="item_block wow">
    <a href="./news_post.html" class="item_box">
      <div class="item_info clearfix">
        <p class="title fl ellipsis">
          个人、企业网站建设常见问题 Q&A
        </p>
        <div class="date_wrap fr">
          <span class="year">2016</span>
          <i class="time-connect">-</i>
          <span class="m">12</span>
          <i class="time-connect">-</i>
          <span class="d">14</span>
        </div>
      </div>
      <div class="item_des">
        <p>
          根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
        </p>
      </div>
    </a>
  </div>
  <div class="item_block wow">
    <a href="./news_post.html" class="item_box">
      <div class="item_info clearfix">
        <p class="title fl ellipsis">
          个人、企业网站建设常见问题 Q&A
        </p>
        <div class="date_wrap fr">
          <span class="year">2016</span>
          <i class="time-connect">-</i>
          <span class="m">12</span>
          <i class="time-connect">-</i>
          <span class="d">14</span>
        </div>
      </div>
      <div class="item_des">
        <p>
          根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
        </p>
      </div>
    </a>
  </div>
</div>