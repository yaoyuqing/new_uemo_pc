<!-- 底部 -->
<div class="popup_qr pop-wechat">
  <div class="fixed-container wx_qrcode">
      <div class="qrcode"><img src="../assets/images/qrcode.png" alt="">
          <p>扫描二维码关注公众号</p>
      </div>
  </div>
</div>
<footer id="footer" data-static="/">
  <div class="module_container">
    <div class="foot_nav wow fadeInUp">
      <a href="#" target="_blannk">关于我们</a>
      <a href="/document/doc/" target="_blannk">相关教程</a>
      <a href="/document/faq/" target="_blannk">常见问题</a>
      <a href="" target="_blannk">法律声明</a>
      <a href="#" target="_blannk">联系我们</a>
    </div>
    <div class="copyright wow fadeInUp">COPYRIGHT (©) 2020 企业快速建站-免费自助模板建站-高端网站定制设计-魔艺建站专业网站建设公司.</div>
    <div class="beian wow fadeInUp">
      <a rel="nofollow" class="beian" href="http://beian.miit.gov.cn" target="_blank">京ICP备12019481号-5</a>
      <a href="//resources.jsmo.xin/templates/upload/2/copy-gov.jpg" target="_blank">增值电信业务经营许可证：京B2-20181555</a>
    </div>
    <div class="friendsLink wow fadeInUp">
      <span class="title">友情链接：</span>
      <a href="#">企业建站</a>
      <a href="#">68design</a>
      <a href="#">网站建设</a>
      <a href="#">牛大拿</a>
      <a href="#">网页设计</a>
      <a href="#">设计师网址导航</a>
      <a href="#">优艺客uelike</a>
      <a href="#">腾讯TGideas</a>
      <a href="#">一番一站</a>
    </div>
    <div class="contact wow fadeInUp" data-wow-dalay="0.1s">
      <div class="share">
        <a href="javascript:;" class="active wechat open-popup" data-type="wechat">
          <i class="iconfont icon-weixin1"></i>
        </a>
        <a href="#" class="weibo">
          <i class="iconfont icon-weibo1"></i>
        </a>
      </div>
      <div class="tel">
        <div class="num">010-69557550</div>
        <span class="text">周一至周五 9:30 - 18:30</span>
      </div>
    </div>
  </div>
</footer>
<div class="notice-popup">
  <div class="notice-wrapper">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        <a class="swiper-slide" href="https://www.uemo.net/?debug=1" target="_blank">
          <p class="ellipsis">1新品发布：好看智能名片 —— 品牌营销拓客工具,全员营销利器！可直接绑定UEmo建站产品</p>
        </a>
        <a class="swiper-slide" href="https://www.uemo.net/" target="_blank">
          <p class="ellipsis">2新品发布：好看智能名片 —— 品牌营销拓客工具,全员营销利器！可直接绑定UEmo建站产品</p>
        </a>
      </div>
    </div>
    <a href="javascript:;" target="_blank" class="more">了解详情</a>
  </div>
</div>
</body>

</html>