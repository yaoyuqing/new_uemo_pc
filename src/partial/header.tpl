<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8" />
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{ title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  </head>

  <body class="{{ bodyClass }}">
    <!-- 头部 -->
    <header id="header" class="index_nav" style="position: fixed;">
      <div class="content">
        <div class="header_left flex">
          <div class="drop">
            <a id="logo" href="/"><i class="iconfont icon-uemologo-copy"></i></a>
            <div class="drop_title">
              <span class="text">优艺客旗下品牌</span>
              <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
            </div>
            <ul class="drop_list">
              <li class="drop_item active">
                <a href="https://www.uemo.net/?backend=old_templates_to_use">
                  <span class="item_text">旧版UEMO建站</span>
                  <i class="item_right iconfont icon-youjiantou"></i>
                </a>
              </li>
              <li class="drop_item">
                <a href="http://www.uedna.com/">
                  <span class="item_text">设计师交流平台</span>
                  <i class="item_right iconfont icon-youjiantou"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <ul id="nav" class="flex">
          <li class="navitem">
            <a class="nav-a link_box {{if active === 'template'}} active {{/if}}" href="./template.html" target="_self" data-href="template">
              <span data-title="模板">模板</span>
            </a>
          </li>
          <li class="navitem subnav">
            <a class="nav-a link_box {{if active === 'case'}} active {{/if}}" data-href="customer" href="javascript:;" target="">
              <span data-title="案例">案例</span><i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
            </a>
          </li>
          <li class="navitem">
            <a class="nav-a link_box {{if active === 'client'}} active {{/if}}" href="./client.html" target="_self" data-href="client">
              <span data-title="客户">客户</span>
            </a>
          </li>
          <li class="navitem">
            <a class="nav-a link_box {{if active === 'price'}} active {{/if}}" href="./price.html" target="_self"  data-href="price">
              <span data-title="价格">价格</span>
            </a>
          </li>
          <li class="navitem">
            <a class="nav-a link_box {{if active === 'agent'}} active {{/if}}" href="./agent.html" target="_self" data-href="agent">
              <span data-title="代理加盟">代理加盟</span>
            </a>
          </li>
          <li class="navitem">
            <a class="nav-a link_box {{if active === 'news'}} active {{/if}}" href="./news.html" target="_self" data-href="document">
              <span data-title="帮助">帮助</span>
            </a>
          </li>
        </ul>
        <div class="header_right flex">
          <div class="search_btn">
            <i class="iconfont icon-sousuo-copy"></i>
          </div>
          <div class="login-btn hide">
            <a class="r_animate1 sign_in" href="#">登录</a>
            <a class="r_animate2 sign_up" href="#">注册</a>
          </div>
          <div class="manage-box flex hide">
            <div class="notice r_animate1 fl">
              <i class="iconfont icon-tongzhi1"></i>
              <span class="num hide">2</span>
            </div>
            <div class="head r_animate2">
              <a href="javascript:;">
                <img src="../assets/images/head.jpg" alt="" />
                <span class="id ellipsis">1381234567</span>
                <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
              </a>
              <div class="manage-cont">
                <div class="item_manage manage_info">
                  <div class="user_info">
                    <div class="name">
                      北京优艺客文化传播有限公司
                    </div>
                    <div class="level">
                      <span>等级：</span>
                      <span class="lv">普通</span>
                    </div>
                  </div>
                  <div class="logout">
                    <a href="#">退出</a>
                  </div>
                </div>
                <div class="item_manage manage_select">
                  <a href="#">个人中心</a>
                  <a href="#">账号设置</a>
                </div>
                <div class="item_manage manage_product clearfix">
                  <div>
                    <a href="#">
                      <i class="iconfont icon-shejiyukaifa-2"></i>
                      <span>UEmo模板</span>
                    </a>
                  </div>
                  <div>
                    <a href="#">
                      <i class="iconfont icon-shejiyukaifa-"></i>
                      <span>单页产品</span>
                    </a>
                  </div>
                  <div>
                    <a href="#">
                      <i class="iconfont icon-kexuejishu-13"></i>
                      <span>设计工具</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <aside id="searchview" class="searchview search_box">
        <div class="search_mask"></div>
        <div class="searchview_wrapper">
          <div class="search_content">
            <div class="search_bar">
              <div class="search_bar_wrapper">
                <form action="" id="searchform">
                  <input type="text" placeholder="搜索" class="searchform_input" />
                  <button id="searchform_submit" class="searchform_submit">
                    <i class="iconfont icon-sousuo-copy"></i>
                  </button>
                </form>
              </div>
            </div>
            <div class="search_results">
              <ul class="drop-list">
                <li>案例</li>
                <li>案例实例</li>
                <li>案例价格</li>
              </ul>
            </div>
            <div class="search_keywords">
              <section class="result_block history_word">
                <div class="clear_keyword">清空</div>
                <h4>最近搜索</h4>
                <div class="result_words">
                  <a href="#">网站备案</a>
                  <a href="#" class="active">模板</a>
                  <a href="#">价格</a>
                </div>
              </section>
              <section class="result_block hot_word">
                <h4>热门搜索</h4>
                <div class="result_words">
                  <a href="#">如何解绑域名</a>
                  <a href="#">域名解析流程</a>
                  <a href="#">建站前的准备工作</a>
                </div>
              </section>
            </div>
          </div>
          <div class="search_close">
            <div class="search_close_wrapper">
              <div class="line top">
                <span class="rect top"></span>
              </div>
              <div class="line bottom">
                <span class="rect bottom"></span>
              </div>
            </div>
          </div>
        </div>
      </aside>
      <div class="subnav-wrapper">
        <div class="subnav_bg">
          <div class="sub-banner sketch-wrapper">
            <script>
                var alert_imgs = '[{"head_img":"http://10.0.0.6:8080/assets/staticfile/images/sub-img1.jpg","url":""},{"head_img":"http://10.0.0.6:8080/assets/staticfile/images/sub-img2.jpg","url":"https:\/\/www.uemox.com"},{"head_img":"http://10.0.0.6:8080/assets/staticfile/images/sub-img1.jpg","url":"https:\/\/www.uemox.com"}]'
            </script>
            <div class="sketch">
              <a class="link_box" href="javascritp:;" target="_blank"></a>
            </div>
          </div>
          <ul class="subnav">
            <li>
              <div class="head">
                <p class="title">模板案例</p>
                <p class="subtitle">精品网站案例推荐</p>
              </div>
              <div class="con">
                <div class="item-ver col2">
                  <a href="./template_case.html" class="{{if active === 'case'}} active {{/if}}item-con">
                    <span data-text="模板案例推荐"></span>
                  </a>
                  <div class="{{if active === 'case'}} active {{/if}}item-con">
                    <a href="./template_case.html" class="item-con">
                      <span data-text="半定制模板"></span>  
                    </a>
                    <a href="https://www.uemo.net/page/diy.html" target="_blank" class="icon">?</a>
                  </div>
                </div>
                <div class="item-ver col2">
                </div>
              </div>
            </li>
            <li>
              <div class="head">
                <p class="title">定制案例</p>
                <p class="subtitle">国内外多次获奖经历</p>
              </div>
              <div class="con">
                <div class="item-ver col2">
                  <a href="./custom_case.html" class="{{if active === 'case'}} active {{/if}}item-con">
                    <span data-text="全部案例"></span>
                  </a>
                  <a href="./custom_case.html" class="{{if active === 'case'}} active {{/if}}item-con">
                    <span data-text="国内案例"></span>
                  </a>
                </div>
                <div class="item-ver col3">
                  <a href="./custom_case.html" class="{{if active === 'case'}} active {{/if}}item-con">
                    <span data-text="国外案例"></span>
                  </a>
                </div>
                <div class="item-ver col3">
                </div>
              </div>
            </li>
            <li>
              <div class="head">
                <p class="title icon-new">小程序</p>
                <p class="subtitle">品牌营销拓客工具</p>
              </div>
              <div class="con">
                <div class="item-ver col2">
                  <a href="./miniapp_case.html" class="{{if active === 'case'}} active {{/if}}item-con">
                    <span data-text="推荐客户"></span>
                  </a>
                  <a href="javascript:;" class="item-con sub-code">
                    <span data-text="免费试用"></span>
                    <img src="../assets//images/miniapp-code.png" alt="" class="qrcode">
                  </a>
                </div>
                <div class="item-ver col3">
                </div>
                <div class="item-ver col3">
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </header>