{{include './partial/header.tpl' data={title:'uemo模板列表',active:'agent',bodyClass:'bodyagent'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage">
    <div class="banner">
      <div class="rotation">
        <svg class="main1" width="100%" height="100%">
          <defs>
            <linearGradient id="grad1" x1="50%" y1="0%" x2="50%" y2="100%">
              <stop
                offset="10%"
                style="stop-color: rgb(255, 255, 255); stop-opacity: 0.9"
              />
              <stop
                offset="99%"
                style="stop-color: rgb(255, 255, 255); stop-opacity: 0.1"
              />
            </linearGradient>
            <linearGradient id="grad2" x1="50%" y1="0%" x2="50%" y2="100%">
              <stop
                offset="25%"
                style="stop-color: rgb(105, 77, 255); stop-opacity: 1"
              />
              <stop
                offset="99%"
                style="stop-color: rgb(105, 77, 255); stop-opacity: 1"
              />
            </linearGradient>
            <linearGradient id="grad3" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(236, 77, 255)" />
              <stop offset="100%" style="stop-color: rgb(255, 0, 252)" />
            </linearGradient>
            <linearGradient id="grad4" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(15, 219, 102)" />
              <stop offset="100%" style="stop-color: rgb(139, 224, 101)" />
            </linearGradient>
            <linearGradient id="grad5" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(218, 115, 216)" />
              <stop offset="100%" style="stop-color: rgb(249, 138, 172)" />
            </linearGradient>
            <linearGradient id="grad6" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(50, 161, 255)" />
              <stop offset="100%" style="stop-color: rgb(94, 212, 255)" />
            </linearGradient>
            <linearGradient id="grad7" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(255, 171, 71)" />
              <stop offset="100%" style="stop-color: rgb(255, 189, 110)" />
            </linearGradient>
            <linearGradient id="grad8" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(15, 219, 102)" />
              <stop offset="100%" style="stop-color: rgb(139, 224, 101)" />
            </linearGradient>
            <linearGradient id="grad9" gradientTransform="rotate(0)">
              <stop offset="0%" style="stop-color: rgb(255, 171, 71)" />
              <stop offset="100%" style="stop-color: rgb(255, 189, 110)" />
            </linearGradient>
          </defs>

          <rect class="m1Bg" fill="url(#grad2)" width="100%" height="100%" />

          <g class="m1_stage">
            <g class="m1_cGroup">
              <circle
                class="c1_line c1_line4"
                cx="0"
                cy="50"
                r="550"
                fill="none"
                stroke-width="2"
                stroke="url(#grad1)"
                opacity="0.4"
              />
              <g class="m1Orb orb4">
                <circle cx="70" cy="60" r="86" fill="url('#grad3')" />
              </g>
              <g class="m1Orb orb4a">
                <circle cx="22" cy="6" r="36" fill="url('#grad4')" />
              </g>
              <g class="m1Orb orb4b">
                <circle cx="20" cy="-18" r="34" fill="url('#grad5')" />
              </g>
            </g>
            <g class="m1_cGroup">
              <circle
                class="c1_line c1_line3"
                cx="0"
                cy="50"
                r="450"
                fill="none"
                stroke-width="2"
                stroke="url(#grad1)"
                opacity="0.4"
              />
              <g class="m1Orb orb3">
                <circle cx="21" cy="21" r="42" fill="url('#grad6')" />
              </g>
            </g>
            <g class="m1_cGroup">
              <circle
                class="c1_line c1_line2"
                cx="0"
                cy="50"
                r="360"
                fill="none"
                stroke-width="2"
                stroke="url(#grad1)"
                opacity="0.5"
              />
              <g class="m1Orb orb2">
                <circle cx="20" cy="20" r="25" fill="url('#grad7')" />
              </g>
              <g class="m1Orb orb2b">
                <circle cx="5" cy="5" r="18" fill="url('#grad8')" />
              </g>
            </g>
            <g class="m1_cGroup">
              <circle
                class="c1_solid"
                cx="0"
                cy="50"
                r="280"
                fill="url(#grad1)"
                opacity="0.2"
              />
              <circle
                class="c1_line c1_line1"
                cx="0"
                cy="50"
                r="279"
                fill="none"
                stroke-width="3"
                stroke="url(#grad1)"
                opacity="0.5"
              />
              <g class="m1Orb orb1">
                <circle cx="5" cy="5" r="10" fill="url('#grad9')" />
              </g>
            </g>

            <g class="m1_cGroup">
              <circle
                class="c1_solid"
                cx="0"
                cy="50"
                r="220"
                fill="url(#grad1)"
                opacity="0.4"
              />
            </g>
            <g class="m1_cGroup">
              <circle
                class="c1_solid"
                cx="0"
                cy="50"
                r="150"
                fill="url(#grad1)"
                opacity="0.5"
              />
            </g>
            <g class="m1_cGroup">
              <circle
                class="c1_solid"
                cx="0"
                cy="50"
                r="80"
                fill="url(#grad1)"
                opacity="0.6"
              />
            </g>
          </g>
        </svg>
      </div>
      <div class="info wow">
        <div class="title">
          <img src="./assets/images/agent_title.png" alt="">
        </div>
        <div class="subtitle">全国火热招商中</div>
        <div class="sale clearfix">
          <div class="item_sale fl">
            <div class="phone">13121672111</div>
            <div class="name">黄经理</div>
          </div>
          <div class="item_sale fr">
            <div class="phone">1321889333</div>
            <div class="name">杨经理</div>
          </div>
        </div>
      </div>
    </div>
    <div class="advantage">
      <div class="module_container">
        <div class="container_header wow">
          <div class="title">为什么选择我们?</div>
        </div>
        <div class="container_content">
          <div class="content_list clearfix">
            <div class="item_block wow">
              <div class="item_icon">
                <i class="iconfont icon-pinpaibaozheng"></i>
              </div>
              <div class="item_wrapper">
                <p class="title">品牌保证</p>
                <div class="desc">
                  <p>
                    创始人多次获得国际奖项，作品曾斩获全球最权威的Awwwards国际CSS网站最高奖项，10+年的高端企业网站定制经验，曾服务于多家500强企业
                  </p>
                </div>
              </div>
            </div>
            <div class="item_block wow" data-wow-delay="0.1s">
              <div class="item_icon">
                <i class="iconfont icon-zhinengjianzhan"></i>
              </div>
              <div class="item_wrapper">
                <p class="title">智能建站、零基础建站</p>
                <div class="desc">
                  <p>
                    具备完善智能的系统，无需任何代码编程基础，多款网站成品可选，均适配PC+手机，做到同数据、同网址，但不同体验的终端行为
                  </p>
                </div>
              </div>
            </div>
            <div class="item_block wow" data-wow-delay="0.2s">
              <div class="item_icon">
                <i class="iconfont icon-gaoxingjiabi"></i>
              </div>
              <div class="item_wrapper">
                <p class="title">高性价比</p>
                <div class="desc">
                  <p>
                    加盟即可享有更优惠、更直接的价格，为广大分销伙伴谋求更高利润，多款网站成品可选，真正做到花钱少，功能多、性价比高
                  </p>
                </div>
              </div>
            </div>
            <div class="item_block wow">
              <div class="item_icon">
                <i class="iconfont icon-mianfeipeixun"></i>
              </div>
              <div class="item_wrapper">
                <p class="title">免费培训</p>
                <div class="desc">
                  <p>
                    专属人员提供员工代培训，为您提供平台产品操作的详细视频教程及常见问题培训，全方位扶持合作伙伴，让您得心应手的开展业务
                  </p>
                </div>
              </div>
            </div>
            <div class="item_block wow" data-wow-delay="0.1s">
              <div class="item_icon">
                <i class="iconfont icon-shishigengxin"></i>
              </div>
              <div class="item_wrapper">
                <p class="title">实时更新、迭代升级</p>
                <div class="desc">
                  <p>
                    UEmo产品一直在功能、设计、体验等方面不断迭代及优化技术水平与产品质量稳定，让您无后顾之忧
                  </p>
                </div>
              </div>
            </div>
            <div class="item_block wow" data-wow-delay="0.2s">
              <div class="item_icon">
                <i class="iconfont icon-dunpai"></i>
              </div>
              <div class="item_wrapper">
                <p class="title">稳定的数据/金牌保障</p>
                <div class="desc">
                  <p>
                    用户数据在国内值得信赖的服务器商阿里云，核心安全防护能力，超大规模数据中心遍布全球，安全稳定、灵活高效，因为专业，所以信赖
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="income">
      <div class="ball-bg">
        <div class="yellowball wow"></div>
        <div class="blueball wow"></div>
      </div>
      <div class="module_container">
        <div class="container_header wow">
          <div class="title">
            卖一套模板能赚
            <a href="javascript:;" class="svg_text wowcb">
              几份钱？
              <svg viewBox="0 0 70 36">
                <path
                  d="M6.9739 30.8153H63.0244C65.5269 30.8152 75.5358 -3.68471 35.4998 2.81531C-16.1598 11.2025 0.894099 33.9766 26.9922 34.3153C104.062 35.3153 54.5169 -6.68469 23.489 9.31527"
                />
              </svg>
            </a>
          </div>
          <div class="subtitle">总收入轻松翻几翻</div>
        </div>
        <div class="container_content wow">
          <div class="content_wrapper">
            <div class="trajectory">
              <div class="item_traj out"></div>
              <div class="item_traj mid"></div>
              <div class="item_traj inner"></div>
            </div>
            <div class="contral">
              <label class="btn btn1" for="btn1">
                <div class="preview"></div>
                <div class="info">
                  <div class="number">
                     <span class="unit">￥</span>
                    <span class="text">1000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">基础服务收入</div>
                </div>
              </label>
              <label class="btn btn2" for="btn2">
                <div class="info">
                  <div class="number">
                    <span class="text">5000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">网站优化收入</div>
                </div>
                <div class="preview"></div>
              </label>
              <label class="btn btn3" for="btn3">
                <div class="preview"></div>
                <div class="info">
                  <div class="number">
                    <span class="text">4000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">填充网站收入</div>
                </div>
              </label>
              <label class="btn btn4" for="btn4">
                <div class="preview"></div>
                <div class="info">
                  <div class="number">
                     <span class="unit">￥</span>
                    <span class="text">500
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">每年续费收入</div>
                </div>
              </label>
              <label class="btn btn5" for="btn5">
                <div class="preview"></div>
                <div class="info">
                  <div class="number">
                     <span class="unit">￥</span>
                    <span class="text">8000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">额外定制收入</div>
                </div>
              </label>
            </div>
            <input class="btn_input" id="btn1" name="planet" type="radio" />
            <input class="btn_input" id="btn2" name="planet" type="radio" />
            <input class="btn_input" id="btn3" name="planet" type="radio" />
            <input class="btn_input" id="btn4" name="planet" type="radio" />
            <input class="btn_input" id="btn5" name="planet" type="radio" />
            <div class="ball">
              <div class="item_ball">
                <div class="bg"></div>
                <div class="info">
                  <div class="number">
                     <span class="unit">￥</span>
                    <span class="text">1000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">基础服务收入</div>
                  <div class="tip">购买后直接开通即可</div>
                </div>
                <div class="overlay"></div>
              </div>
              <div class="item_ball">
                <div class="bg"></div>
                <div class="info">
                  <div class="number">
                    <span class="text">5000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">网站优化收入</div>
                  <div class="tip">购买后直接开通即可</div>
                </div>
                <div class="overlay"></div>
              </div>
              <div class="item_ball">
                <div class="bg"></div>
                <div class="info">
                  <div class="number">
                    <span class="text">4000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">填充网站收入</div>
                  <div class="tip">购买后直接开通即可</div>
                </div>
                <div class="overlay"></div>
              </div>
              <div class="item_ball">
                <div class="bg"></div>
                <div class="info">
                  <div class="number">
                     <span class="unit">￥</span>
                    <span class="text">500
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">每年续费收入</div>
                  <div class="tip">购买后直接开通即可</div>
                </div>
                <div class="overlay"></div>
              </div>
              <div class="item_ball">
                <div class="bg"></div>
                <div class="info">
                  <div class="number">
                     <span class="unit">￥</span>
                    <span class="text">8000
                      <span class="cross"></span>
                    </span>
                  </div>
                  <div class="title">额外定制收入</div>
                  <div class="tip">购买后直接开通即可</div>
                </div>
                <div class="overlay"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="process">
      <div class="module_container">
        <div class="container_header wow">
          <div class="title">加盟流程</div>
        </div>
        <div class="container_content">
          <div class="content_list clearfix">
            <div class="item_block active clearfix wow">
              <div class="item_index">01</div>
              <div class="item_icon fl">
                <svg>
                  <use xlink:href="#icon-01" class="icon" />
                  <use xlink:href="#icon-01" class="icon-border" />
                  <use xlink:href="#icon-01-fill" class="icon-fill" />
                </svg>
              </div>
              <div class="item_wrapper fl">
                <p class="title">洽谈加盟</p>
                <p class="subtitle">洽谈具体细节，专属客服对接</p>
              </div>
            </div>
            <div class="item_block clearfix wow"  data-wow-delay="0.1s">
              <div class="item_index">02</div>
              <div class="item_icon fl">
                <svg>
                  <use xlink:href="#icon-02" class="icon" />
                  <use xlink:href="#icon-02" class="icon-border" />
                  <use xlink:href="#icon-02-fill" class="icon-fill" />
                </svg>
              </div>
              <div class="item_wrapper fl">
                <p class="title">签署合同</p>
                <p class="subtitle">核对具体细节，专属客服对接</p>
              </div>
            </div>
            <div class="item_block clearfix wow"  data-wow-delay="0.2s">
              <div class="item_index">03</div>
              <div class="item_icon fl">
                <svg>
                  <use xlink:href="#icon-03" class="icon" />
                  <use xlink:href="#icon-03" class="icon-border" />
                  <use xlink:href="#icon-03-fill" class="icon-fill" />
                </svg>
              </div>
              <div class="item_wrapper fl">
                <p class="title">开通账户</p>
                <p class="subtitle">专属客服后期服务对接</p>
              </div>
            </div>
            <div class="item_block clearfix wow"  data-wow-delay="0.3s">
              <div class="item_index">04</div>
              <div class="item_icon fl">
                <svg>
                  <use xlink:href="#icon-04" class="icon" />
                  <use xlink:href="#icon-04" class="icon-border" />
                  <use xlink:href="#icon-04-fill" class="icon-fill" />
                </svg>
              </div>
              <div class="item_wrapper fl">
                <p class="title">合作共赢</p>
                <p class="subtitle">达成合作，携手共创文字占位</p>
              </div>
            </div>
          </div>
          <!-- SVG -->
          <svg style="display: none">
            <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.661 60.082" id="icon-01">
              <path id="路径_30" data-name="路径 30" d="M49.012,48.057a15.836,15.836,0,0,0,8.414-13.785,15.558,15.558,0,0,0-4.03-9.845c-.06-.07.173-.856.152-2.127a18.921,18.921,0,0,0-.253-2.339,19.255,19.255,0,0,1,7.2,14.489c0,6.21-3.394,11.724-8.655,15.277v8.49a.668.668,0,0,1-1.015.571l-8.688-5.273a26.093,26.093,0,0,1-4.145.344c-6.49,0-10.679-2.383-14.7-6.175a.166.166,0,0,1,.123-.285c.528.031,1.056.061,1.594.061.966,0,1.92-.047,2.863-.122,3.254,2.206,5.636,3.534,10.119,3.534a22.181,22.181,0,0,0,4.478-.47l5.981,3.339c.611.288.508.222.508-.338ZM29.334,44.048a51.4,51.4,0,0,1-5.526-.41.892.892,0,0,0-.556.109L15.1,48.4a.9.9,0,0,1-1.343-.78V39.9a.9.9,0,0,0-.312-.678,19.293,19.293,0,0,1-6.612-15.01C6.831,13.494,16.889,4.8,29.334,4.8c10.346,0,20.772,8.69,20.772,19.409S41.762,44.048,29.334,44.048Zm-6.21-3.882h0a42.736,42.736,0,0,0,6.21.469c10.516,0,17.31-7.257,17.31-16.209s-8.7-16.209-17.31-16.209c-10.814,0-19.041,7.257-19.041,16.209,0,5.688,2.7,10.2,6.924,13.576h0l-.3,6.046Zm14.941-13.18A2.57,2.57,0,0,1,35.7,23.507a2.443,2.443,0,0,1,1.535-1.5,2.563,2.563,0,1,1,.826,4.978Zm-9.6,0a2.57,2.57,0,0,1-2.361-3.478,2.442,2.442,0,0,1,1.535-1.5,2.563,2.563,0,1,1,.827,4.978Zm-9.6,0a2.569,2.569,0,0,1-2.361-3.479,2.443,2.443,0,0,1,1.535-1.5,2.563,2.563,0,1,1,.826,4.978Z"/>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.661 60.082" id="icon-01-fill">
                <path d="M29.334,44.048a51.4,51.4,0,0,1-5.526-.41.892.892,0,0,0-.556.109L15.1,48.4a.9.9,0,0,1-1.343-.78V39.9a.9.9,0,0,0-.312-.678,19.293,19.293,0,0,1-6.612-15.01C6.831,13.494,16.889,4.8,29.334,4.8c10.346,0,20.772,8.69,20.772,19.409S41.762,44.048,29.334,44.048Z" fill="var(--primary)"/>
                <path id="路径_33" data-name="路径 33" d="M38.065,26.985A2.57,2.57,0,0,1,35.7,23.507a2.443,2.443,0,0,1,1.535-1.5,2.563,2.563,0,1,1,.826,4.978Zm-9.6,0a2.57,2.57,0,0,1-2.361-3.478,2.442,2.442,0,0,1,1.535-1.5,2.563,2.563,0,1,1,.827,4.978Zm-9.6,0a2.569,2.569,0,0,1-2.361-3.479,2.443,2.443,0,0,1,1.535-1.5,2.563,2.563,0,1,1,.826,4.978Z" fill="var(--secondary)"/>
                <path id="路径_34" data-name="路径 34" d="M49.012,48.057a15.836,15.836,0,0,0,8.414-13.785,15.558,15.558,0,0,0-4.03-9.845c-.06-.07.173-.856.152-2.127a18.921,18.921,0,0,0-.253-2.339,19.255,19.255,0,0,1,7.2,14.489c0,6.21-3.394,11.724-8.655,15.277v8.49a.668.668,0,0,1-1.015.571l-8.688-5.273a26.093,26.093,0,0,1-4.145.344c-6.49,0-10.679-2.383-14.7-6.175a.166.166,0,0,1,.123-.285c.528.031,1.056.061,1.594.061.966,0,1.92-.047,2.863-.122,3.254,2.206,5.636,3.534,10.119,3.534a22.181,22.181,0,0,0,4.478-.47l5.981,3.339c.611.288.508.222.508-.338Z" fill="var(--primary)"/>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" id="icon-02">
                <g xmlns="http://www.w3.org/2000/svg" id="组_4" data-name="组 4" transform="translate(-792 -810)">
                  <path id="路径_15" data-name="路径 15" d="M93.094,121.919a2.079,2.079,0,0,1-1.729,2.052V124H76.706A3.816,3.816,0,0,1,73,120.09V75.408A3.817,3.817,0,0,1,76.706,71.5h41.125a3.814,3.814,0,0,1,3.705,3.908V95.951a1.975,1.975,0,1,1-3.95,0V86.144c0-.04,0-.079,0-.119L117.5,75.6l-40.363.109L77.015,119.9l4.054-.018a2,2,0,0,1,.433-.047h9.863v.027a2.078,2.078,0,0,1,1.729,2.052ZM83.93,84.689a2.189,2.189,0,0,0,0,4.377h26.839a2.189,2.189,0,0,0,0-4.377ZM99.243,96.433a2.183,2.183,0,0,0-2.164-2.188H83.889a2.189,2.189,0,0,0,0,4.377h13.19A2.183,2.183,0,0,0,99.243,96.433Zm-6.06,7.34h-9.32a2.189,2.189,0,0,0,0,4.377h9.32a2.189,2.189,0,0,0,0-4.377Zm24.765,7.561a2.37,2.37,0,0,1-.606,2.614l-1.5,1.453.352,2.034a2.3,2.3,0,0,1-.958,2.292,2.34,2.34,0,0,1-2.426,0l-1.724-.9-1.723.936a2.34,2.34,0,0,1-2.426,0,2.459,2.459,0,0,1-.574-.582,2.209,2.209,0,0,1-.384-1.71l.32-2.034-1.5-1.485a2.279,2.279,0,0,1-.543-2.389,2.307,2.307,0,0,1,1.852-1.517l2.011-.291.862-1.775a2.331,2.331,0,0,1,2.107-1.324h.32a2.2,2.2,0,0,1,1.787,1.259l.862,1.614,2.044.29a2.307,2.307,0,0,1,1.849,1.519Zm-3.013,1.179-2.1-.31a1.18,1.18,0,0,1-.811-.594l-.921-1.921-.95,1.921a1.06,1.06,0,0,1-.782.594l-2.123.31,1.536,1.5a1.082,1.082,0,0,1,.307.933l-.363,2.146,1.872-1.017a1.049,1.049,0,0,1,1,0l1.872.989-.363-2.119a1.079,1.079,0,0,1,.307-.932Zm10.065.98a13.853,13.853,0,1,1-13.853-14.007A13.931,13.931,0,0,1,125,113.493Zm-3.454-.1A10.449,10.449,0,1,0,111.1,123.96a10.507,10.507,0,0,0,10.448-10.565Z" transform="translate(719 742.5)"/>
                </g>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" id="icon-02-fill">
                <g xmlns="http://www.w3.org/2000/svg" id="组_4" data-name="组 4" transform="translate(-792 -810)">
                  <path id="路径_20" data-name="路径 20" d="M83.93,84.689a2.189,2.189,0,0,0,0,4.377h26.839a2.189,2.189,0,0,0,0-4.377ZM99.243,96.433a2.183,2.183,0,0,0-2.164-2.188H83.889a2.189,2.189,0,0,0,0,4.377h13.19A2.183,2.183,0,0,0,99.243,96.433Zm-6.06,7.34h-9.32a2.189,2.189,0,0,0,0,4.377h9.32a2.189,2.189,0,0,0,0-4.377Z" transform="translate(719 742.5)" fill="var(--primary)"/>
                  <path id="路径_17" data-name="路径 17" d="M125,113.493a13.853,13.853,0,1,1-13.853-14.007A13.931,13.931,0,0,1,125,113.493Z" transform="translate(719.004 742.514)" fill="var(--primary)"/>
                  <path id="路径_18" data-name="路径 18" d="M117.948,111.334a2.37,2.37,0,0,1-.606,2.614l-1.5,1.453.352,2.034a2.3,2.3,0,0,1-.958,2.292,2.34,2.34,0,0,1-2.426,0l-1.724-.9-1.723.936a2.34,2.34,0,0,1-2.426,0,2.459,2.459,0,0,1-.574-.582,2.209,2.209,0,0,1-.384-1.71l.32-2.034-1.5-1.485a2.279,2.279,0,0,1-.543-2.389,2.307,2.307,0,0,1,1.852-1.517l2.011-.291.862-1.775a2.331,2.331,0,0,1,2.107-1.324h.32a2.2,2.2,0,0,1,1.787,1.259l.862,1.614,2.044.29a2.307,2.307,0,0,1,1.849,1.519Zm-3.013,1.179-2.1-.31a1.18,1.18,0,0,1-.811-.594l-.921-1.921-.95,1.921a1.06,1.06,0,0,1-.782.594l-2.123.31,1.536,1.5a1.082,1.082,0,0,1,.307.933l-.363,2.146,1.872-1.017a1.049,1.049,0,0,1,1,0l1.872.989-.363-2.119a1.079,1.079,0,0,1,.307-.932Z" transform="translate(719.179 742.348)" fill="var(--secondary)"/>
                  <path id="路径_21" data-name="路径 21" d="M93.094,121.919a2.079,2.079,0,0,1-1.729,2.052V124H76.706A3.816,3.816,0,0,1,73,120.09V75.408A3.817,3.817,0,0,1,76.706,71.5h41.125a3.814,3.814,0,0,1,3.705,3.908V95.951a1.975,1.975,0,1,1-3.95,0V86.144c0-.04,0-.079,0-.119L117.5,75.6l-40.363.109L77.015,119.9l4.054-.018a2,2,0,0,1,.433-.047h9.863v.027a2.078,2.078,0,0,1,1.729,2.052Z" transform="translate(719 742.5)" fill="var(--primary)"/>
                </g>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.352 60.352" id="icon-03">
              <g xmlns="http://www.w3.org/2000/svg" id="组_5" data-name="组 5" transform="translate(-790 -954)">
                <path id="路径_35" data-name="路径 35" d="M102.2,68.4C83.034,68.4,68.425,88.376,78.1,108.6a23.594,23.594,0,0,0,11.09,11.09c20.223,9.672,40.194-4.937,40.194-24.108a27.123,27.123,0,0,0-7.517-18.743.658.658,0,0,1,.006-.921l2.563-2.562a.559.559,0,0,0-.3-.946l-10.455-1.745a.736.736,0,0,0-.847.847l1.739,10.446a.565.565,0,0,0,.957.307L118,79.782a.655.655,0,0,1,.94.019,23,23,0,0,1-16.67,38.776H102.2a22.965,22.965,0,0,1-23-22.93V95.58a22.965,22.965,0,0,1,22.93-23h.068Zm.014,12.608A9.006,9.006,0,0,0,96.012,96.55a.4.4,0,0,1-.075.632,12.7,12.7,0,0,0-6.336,11h3.605a9.007,9.007,0,0,1,8.95-9.061h.062a9.006,9.006,0,0,1,9.012,9v.058h3.6a12.7,12.7,0,0,0-6.345-11.008.388.388,0,0,1-.074-.615,9,9,0,0,0-6.2-15.545Zm0,3.621a5.435,5.435,0,1,1-5.407,5.489v-.052a5.394,5.394,0,0,1,5.407-5.434Z" transform="translate(714.971 889.596)"/>
              </g>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.352 60.352" id="icon-03-fill">
              <g xmlns="http://www.w3.org/2000/svg" id="组_5" data-name="组 5" transform="translate(-790 -954)">
                <path xmlns="http://www.w3.org/2000/svg" id="路径_37" data-name="路径 37" d="M102.2,68.4C83.034,68.4,68.425,88.376,78.1,108.6a23.594,23.594,0,0,0,11.09,11.09c20.223,9.672,40.194-4.937,40.194-24.108a27.123,27.123,0,0,0-7.517-18.743.658.658,0,0,1,.006-.921l2.563-2.562a.559.559,0,0,0-.3-.946l-10.455-1.745a.736.736,0,0,0-.847.847l1.739,10.446a.565.565,0,0,0,.957.307L118,79.782a.655.655,0,0,1,.94.019,23,23,0,0,1-16.67,38.776H102.2a22.965,22.965,0,0,1-23-22.93V95.58a22.965,22.965,0,0,1,22.93-23h.068Zm.014,12.608A9.006,9.006,0,0,0,96.012,96.55a.4.4,0,0,1-.075.632,12.7,12.7,0,0,0-6.336,11h3.605c7.871,0,11.745-.061,18.024-.061,0,.019-15.293,0-15.293,0l18.9.058a12.7,12.7,0,0,0-6.345-11.008.388.388,0,0,1-.074-.615,9,9,0,0,0-6.2-15.545Z" transform="translate(714.971 889.596)" fill="var(--primary)"/>
              </g>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" id="icon-04">
              <g xmlns="http://www.w3.org/2000/svg" id="组_6" data-name="组 6" transform="translate(-787.499 -1105.669)">
                <path id="联合_2" data-name="联合 2" d="M95.836,122.491,74.172,101.2a14.792,14.792,0,0,1-.386-20.917,15.276,15.276,0,0,1,21.521-.207l3.218,3.165,3.789-3.724c5.563-5.466,14.871-5.215,20.746.559s6.134,14.923.568,20.389L103.1,120.645h0l-1.793,1.763a4,4,0,0,1-2.6,1.176q-.084,0-.167,0A3.807,3.807,0,0,1,95.836,122.491ZM76.6,82.64a11.065,11.065,0,0,0,0,15.817l21.844,21.468a.111.111,0,0,0,.08.033.089.089,0,0,0,.079-.033l3.714-3.65-5.618-5.84a1.789,1.789,0,0,1,.07-2.56l.005-.005a1.864,1.864,0,0,1,2.6.072l0,0,5.545,5.766,3.022-2.969-5.509-5.729a1.79,1.79,0,0,1,.07-2.561l0,0a1.869,1.869,0,0,1,2.608.073h0l5.439,5.654,4.058-3.988L92.7,82.64a11.532,11.532,0,0,0-16.095,0Zm28.325-.56L102.7,84.273a2.14,2.14,0,0,0,0,3.065l14.536,14.286,3.79-3.725c4.121-4.051,3.866-10.9-.57-15.258a11.874,11.874,0,0,0-8.287-3.441A10.234,10.234,0,0,0,104.927,82.08Z" transform="translate(718.358 1040.425)"/>
              </g>
            </symbol>
            <symbol xmlns="http://www.w3.org/2000/svg" id="icon-04-fill">
              <g xmlns="http://www.w3.org/2000/svg" id="组_6" data-name="组 6" transform="translate(-787.499 -1105.669)">
                <path id="路径_45" data-name="路径 45" d="M98.525,83.241l-3.218-3.165a15.275,15.275,0,0,0-21.521.207,14.791,14.791,0,0,0,.386,20.917l21.664,21.292a3.815,3.815,0,0,0,2.859,1.092,3.994,3.994,0,0,0,2.6-1.176l1.793-1.763h0l16.681-16.458Z" transform="translate(718.358 1040.425)" fill="var(--primary)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
                <path id="路径_44" data-name="路径 44" d="M104.695,118.067a1.854,1.854,0,0,1-1.341-.568l-5.737-5.965a1.789,1.789,0,0,1,.071-2.56l0,0a1.864,1.864,0,0,1,2.6.072l0,0,5.736,5.965a1.79,1.79,0,0,1-.071,2.561l0,0a1.859,1.859,0,0,1-1.267.5Zm5.74-5.424a1.858,1.858,0,0,1-1.342-.568l-5.736-5.965a1.79,1.79,0,0,1,.071-2.561l0,0a1.868,1.868,0,0,1,2.608.072h0l5.737,5.965a1.79,1.79,0,0,1-.071,2.561l0,0A1.854,1.854,0,0,1,110.435,112.643Z" transform="translate(717.442 1039.326)" fill="var(--secondary)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
                <path id="路径_47" data-name="路径 47" d="M123.06,80.076c-5.875-5.774-15.183-6.025-20.746-.559l-3.789,3.724-3.218-3.165a15.275,15.275,0,0,0-21.521.207,14.791,14.791,0,0,0,.386,20.917l21.664,21.292a3.815,3.815,0,0,0,2.859,1.092,3.994,3.994,0,0,0,2.6-1.176l1.793-1.763h0l20.533-20.18C129.193,95,128.938,85.85,123.06,80.076ZM98.525,119.958a.111.111,0,0,1-.08-.033L76.6,98.457a11.065,11.065,0,0,1,0-15.817,11.533,11.533,0,0,1,16.095,0l21.923,21.546L98.6,119.926a.092.092,0,0,1-.079.033Zm22.5-22.06-3.789,3.725L102.7,87.337a2.14,2.14,0,0,1,0-3.064l2.231-2.193c4.122-4.051,11.086-3.8,15.525.56s4.691,11.207.569,15.258Z" transform="translate(718.358 1040.425)" fill="var(--primary)"/>
              </g>
            </symbol>
          </svg>
        </div>
      </div>
    </div>
    <div class="level">
      <div class="module_container">
        <div class="container_header wow">
          <div class="title">代理级别</div>
          <div class="subtitle">
            代理级别根据预存款项划分等级，该预存款用于开通UEmo产品，不做为保证金、押金、服务费、手续费等
          </div>
        </div>
        <div class="container_content">
          <div class="content_list clearfix">
            <div class="item_block wow">
              <div class="item_info">
                <p class="title">初级代理</p>
                <p class="subtitle">未开放CSS编辑权限</p>
              </div>
              <div class="interest">
                <span class="num">8</span>
                <span class="text">折</span>
              </div>
              <div class="tip">可累计升至中级代理</div>
            </div>
            <div class="item_block wow" data-wow-delay="0.1s">
              <div class="item_info">
                <p class="title">中级代理</p>
                <p class="subtitle">开放CSS编辑权限</p>
              </div>
              <div class="interest">
                <span class="num">5</span>
                <span class="text">折</span>
              </div>
              <div class="tip"></div>
            </div>
            <div class="item_block wow" data-wow-delay="0.2s">
              <div class="item_info">
                <p class="title">高级代理</p>
                <p class="subtitle">（暂未开发）</p>
              </div>
              <div class="interest">
                <i class="iconfont icon-guanbicuowu"></i>
              </div>
              <div class="tip"></div>
            </div>
          </div>
          <div class="try_template wow">
            <div class="try_limit clearfix">
              <div class="item_limit">
                <p>无</p>
                <div>加盟费</div>
              </div>
              <div class="item_limit">
                <p>无</p>
                <div>消费时间限制</div>
              </div>
            </div>
            <div class="try_btn clearfix">
              <div class="tips">
                <p class="info">
                  免费赠送一套模板<span class="text money">2999</span
                  ><span class="text unit">元</span>
                </p>
                <p class="tip">作为代理官网使用（适用于所有代理商）</p>
              </div>
              <a class="link_box btn" target="_blank " href="http://mo005-2232.mo5.line1.jsmo.xin/">
                <span class="text">点击试用代理专用模板</span>
                <svg
                  class="svg_arrow"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink"
                  x="0px"
                  y="0px"
                  viewBox="0 0 43.1 85.9"
                  style="enable-background: new 0 0 43.1 85.9"
                  xml:space="preserve"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="line draw-arrow wowcb"
                    d="M11.3,2.5c-5.8,5-8.7,12.7-9,20.3s2,15.1,5.3,22c6.7,14,18,25.8,31.7,33.1"
                  ></path>
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="draw-arrow tail-1 wowcb"
                    d="M40.6,78.1C39,71.3,37.2,64.6,35.2,58"
                  ></path>
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="draw-arrow tail-2 wowcb"
                    d="M39.8,78.5c-7.2,1.7-14.3,3.3-21.5,4.9"
                  ></path>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}
