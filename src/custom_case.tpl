{{include './partial/header.tpl' data={title:'uemo定制案例',active:'case',bodyClass:'bodycustomcase'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pageList">
    <div id="banner" class="banner">
      <div class="bg">
        <div class="item_img wow" style="background-image:url(<%= require("./assets/images/custom_banner.jpg") %>);">
        </div>
      </div>
      <div class="banner_info">
        <div class="info_left fl">
          <div class="smile wow"></div>
          <span class="text wow" data-wow-delay="0.3s">您给我们信任我们给您惊喜 .</span>
        </div>
        <div class="info_right fr wow">
          <div class="item_block">
            <p class="number">
              <span class="count">13</span>
              <span class="unit">年</span>
            </p>
            <p class="title">建站经验</p>
          </div>
          <div class="item_block">
            <p class="number">
              <span class="count">1200</span>
              <span class="unit cross"></span>
            </p>
            <p class="title">客户案例</p>
          </div>
          <div class="item_block">
              <p class="number">
                  <span class="count">14</span>
                  <span class="unit cross"></span>
              </p>
              <p class="title">覆盖国家及地区</p>
          </div>
        </div>
      </div>
    </div>
    <div id="page_content" class="page_content">
      <div class="mlist customcase">
        <div class="module_container">
          <div class="container_category">
            <div class="title">
              <span class="text">全部案例</span>
              <span class="num">64</span>
              <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
            </div>
          </div>
          <div class="dropdown">
            <div class="title">
              <span class="text">地域</span>
              <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
            </div>
            <div class="dropdown-menu">
              <a href="javascritp::" class="link_box dropdown-item active">全部</a>
              <a href="javascritp::" class="link_box vdropdown-item">国内</a>
              <a href="javascritp::" class="link_box dropdown-item">国外</a>
            </div>
          </div>
          <div class="container_content">
            <div class="content_list clearfix">
              <% for(var i = 0; i < 5; i++){ %>
                <div class="item_block wow view-indicator-show">
                  <a href="./custom_case_post.html" class="item_box">
                    <div class="item_img">
                      <img src="./assets/images/custom_case.jpg" alt="">
                    </div>
                    <div class="item_wrapper">
                      <div class="title">
                        <span class="text ellipsis">ksher</span>
                      </div>
                      <div class="subtitle">
                        <span class="text ellipsis">跨境支付</span>
                      </div>
                    </div>
                  </a>
                </div>
              <% } %>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="overlay">
  <ul class="category-list clearfix">
    <li>
      <a href="#">
        <span class="text">全部</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
    </li>
    <li>
      <a href="#">
        <span class="text">企业集团</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">金融理财</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">影视传媒</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
    </li>
    <li>
      <a href="#">
        <span class="text">建筑园林</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">医疗养生</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">服饰礼服</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
    </li>
    <li>
      <a href="#">
        <span class="text">艺术设计</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">教育机构</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">休闲游戏</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
    </li>
    <li>
      <a href="#">
        <span class="text">餐饮美食</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
      <a href="#">
        <span class="text">其他</span>
        <i class="iconfont icon-youjiantou"></i>
      </a>
    </li>
  </ul>
</div>
<div class="view-indicator">
  <div class="color-circle">
    <i class="iconfont icon-youjiantou"></i>
  </div>
</div>
{{include './partial/footer.tpl'}}