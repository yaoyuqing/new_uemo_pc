{{include './partial/header.tpl' data={title:'uemo帮助中心',active:'news',bodyClass:'bodylist bodynewspost'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
    <div id="pageContent" class="pageContent">
        <div id="pageTarget" class="pageTarget">
            <div class="container_category">
                <div class="category_wrapper">
                    <a href="#" class="active">全部</a>
                    <a href="#">选择模板</a>
                    <a href="#">SEO</a>
                    <a href="#">域名问题</a>
                    <a href="#">特色功能</a>
                    <a href="#">管理网站</a>
                    <a href="#">新闻咨询</a>
                    <a href="#">使用教程</a>
                </div>
            </div>
        </div>
        <div id="postWrapper" class="postWrapper">
            <div class="postContent">
                <div class="postInfo">
                    <p class="title">个人、企业网站建设常见问题 Q&A</p>
                    <p class="subtitle">2020-11-12</p>
                </div>
                <div class="postBody">
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        简单的说凑四，假如您使用没有备案号的域名的话，工信部有！权！随时“和谐”掉您的网站哦！根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        简单的说凑四，假如您使用没有备案号的域名的话，工信部有！权！随时“和谐”掉您的网站哦！根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        简单的说凑四，假如您使用没有备案号的域名的话，工信部有！权！随时“和谐”掉您的网站哦！根据中华人民共和国信息产业部第十二次部务会议审议通过的《非经营性互联网信息服务备案管理办法》条例，在中华人民共和国境内提供非经营性互联网信息服务，应当办理备案。未经备案，不得在中华人民共和国境内从事非经营性互联网信息服务。而对于没有备案的网站将予以罚款或关闭。
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p
                        style="
                            height: 1px;
                            background-color: #f0f0f0;
                            font-size: 14px;
                            line-height: 28px;
                            color: #999;
                        "
                    >
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                    <p style="font-size: 14px; line-height: 28px; color: #999;">
                        <br />
                    </p>
                </div>
            </div>
            <div class="listWrap news">
                <div id="listContent" class="listContent_post clearfix">
                    <h3>相关内容</h3>
                    <div class="item_tags">
                        <a href="#" target="_blank">域名绑定</a>
                        <a href="#" target="_blank">域名绑定</a>
                        <a href="#" target="_blank">域名绑定</a>
                    </div>
                </div>
                <div class="module_container">
                    <div class="container_content">
                        <div class="content_wrapper">
                            {{include './partial/news_list.tpl'}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{include './partial/footer.tpl'}}
