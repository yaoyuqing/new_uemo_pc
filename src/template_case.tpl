{{include './partial/header.tpl' data={title:'uemo模板案例',active:'case',bodyClass:'bodycase bodylist'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pageList">
    <div id="page_content" class="page_content">
      <div class="mlist user">
        <div class="module_container">
          <div class="container_header">
            <p class="title">模板案例推荐</p>
          </div>
          <div class="container_category">
            <div class="category_wrapper swiper-container">
              <div class="swiper-wrapper">
                <a href="#" class="swiper-slide active">全部案例</a>
                <a href="#" class="swiper-slide">艺术设计</a>
                <a href="#" class="swiper-slide">智能科技</a>
                <a href="#" class="swiper-slide">品牌营销</a>
                <a href="#" class="swiper-slide">摄影摄像</a>
                <a href="#" class="swiper-slide">生活家居</a>
                <a href="#" class="swiper-slide">建筑装饰</a>
                <a href="#" class="swiper-slide">企业集团</a>
                <a href="#" class="swiper-slide">医疗教育</a>
                <a href="#" class="swiper-slide">服装饰品</a>
                <a href="#" class="swiper-slide">餐饮美食</a>
                <a href="#" class="swiper-slide">餐饮美食</a>
              </div>
            </div>
            <div class="swiper-button swiper-button-prev iconfont icon-jiantou5-copy-copy"></div>
              <div class="swiper-button swiper-button-next iconfont icon-jiantou5-copy-copy-copy"></div>
          </div>
          <div class="container_content">
            <div class="content_list">
              <div class="item_block">
                <div class="item_img" data-src="./assets/images/case_img1.jpg">
                  <a href="javascript:;" target="_self" class="tag">半定制模板</a>
                </div>
                <div class="item_wrapper">
                  <p class="title">品牌设计行业网站</p>
                  <p class="subtitle">品牌设计 | UI |平面设计 | 企宣</p>
                  <div class="description">
                    PC站+手机站 含8G空间，导航自定义分类、首页模块可自由组合或隐藏可绑定独立域名...
                  </div>
                  <a href="https://www.uemo.net/preview/6.html" target="_blank" class="web_btn link_box">网站预览</a>
                  <div class="mobile_btn">
                    <div class="item_btn">
                      <div class="h5">
                        <i class="iconfont icon-phone"></i>
                        <span class="text">手机H5预览</span>
                      </div>
                      <div class="item_qrcode qrcode_wrapper" data-src="https://new.uemo.net/preview/6.html">
                        <img src="./assets/images/qrcode.png" alt="" class="qrcode" />
                      </div>
                    </div>
                    <div class="item_btn">
                      <div class="miniapp">
                        <i class="iconfont icon-xiaochengxu1"></i>
                        <span class="text">小程序预览</span>
                      </div>
                      <div class="item_qrcode" data-href="https://new.uemo.net/preview/6.html" data-id="364">
                        <img src="./assets/images/qrcode.png" alt=""/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item_block">
                <div class="item_img" data-src="./assets/images/case_img2.jpg">
                </div>
                <div class="item_wrapper">
                  <p class="title">品牌设计行业网站</p>
                  <p class="subtitle">品牌设计 | UI |平面设计 | 企宣</p>
                  <div class="description">
                    PC站+手机站 含8G空间，导航自定义分类、首页模块可自由组合或隐藏可绑定独立域名...
                  </div>
                  <a href="https://www.uemo.net/preview/6.html" class="web_btn link_box">网站预览</a>
                  <div class="mobile_btn">
                    <div class="item_btn">
                      <div class="h5">
                        <i class="iconfont icon-phone"></i>
                        <span class="text">手机H5预览</span>
                      </div>
                      <div class="item_qrcode qrcode_wrapper" data-src="https://new.uemo.net/preview/6.html">
                        <img src="./assets/images/qrcode.png" alt="" class="qrcode" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{include './partial/pages.tpl'}}
          </div>
        </div>
      </div>
      <div class="mlist relevant template">
        <div class="module_container">
          <div class="container_header wow">
            <p class="title">摄影摄像相关模板</p>
            <p class="subtitle">
              超1000家行业佼佼者选择UEMO极速建站
            </p>
          </div>
          <div class="container_content">
            <div class="content_wrapper">
              {{include './partial/template_list.tpl'}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}