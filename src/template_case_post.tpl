<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8" />
    <meta name="divport" content="width=device-width, initial-scale=1.0" />
    <title>Demo</title>
  </head>

  <body class="uemo">
    <section id="control" class="control">
      <div class="control-menu">
        <div class="back">
          <a href="javascript:;">
            <i class="iconfont icon-jiantou5-copy-copy"></i>
            <span>返回</span>
          </a>
        </div>
        <div class="device-select">
          <a href="javascript:;" class="desktop active" data-value="desktop">
            <i class="iconfont icon-desktop" aria-hidden="true"></i>
          </a>
          <a href="javascript:;" class="mobile" data-value="mobile">
            <i class="iconfont icon-phone" aria-hidden="true"></i>
          </a>
        </div>
        <div class="url">
          <a href="#">mo005_21594.mo5.line2.jsmo.xin</a>
        </div>
      </div>
      <div id="logo" class="logo">
        <a href="./index.html">
          <i class="iconfont icon-uemologo-copy"></i>
        </a>
      </div>
      <div id="try_btn" class="try_btn next">
        <a href="./template_post.html">下一个</a>
      </div>
      <div id="try_btn" class="try_btn prev">
        <a href="./template_post.html">上一个</a>
      </div>
      <div id="try_btn" class="try_btn">
        <a href="./template_post.html">试用此模板</a>
      </div>
      <div id="fix_btn" class="fix_btn">
        <a href="javascript:;" class="wx">
          <i class="iconfont icon-weixin1"></i>
          <span>微信客服</span>
        </a>
        <span class="text">根据客户需求修改</span>
        <div class="contact">
          <div class="item_img">
            <img src="./assets/images/qrcode.png" width="132" height="132" alt="" />
            <span>扫描二维码加我微信</span>
          </div>
          <div class="item_info">
            <p class="name">杨经理</p>
            <p class="wechat">
              <span>微信号：</span>
              <span>green13121889333</span>
            </p>
            <p class="tel">
              <span>手机号：</span>
              <span>13121889333</span>
            </p>
          </div>
        </div>
      </div>
    </section>
    <div id="contentArea" class="desktop">
      <div class="area_wrapper">
        <iframe id="contentFrame" class="contentFrame" data-src="http://mo005-22017.mo5.line1.jsmo.xin/"  src="http://mo005-22017.mo5.line1.jsmo.xin/" frameborder="no" border="0" marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>
        <div id="mask"></div>
      </div>
      <div class="mobile-code qrcode_wrapper" data-src="http://mo005-22017.mo5.line1.uemo.net">
        <img src="./assets/images/qrcode.png" width="175" height="175" alt="" class="qrcode" />
        <span class="text">扫描二维码预览手机站</span>
      </div>
      <div class="miniapp-popup">
        <div class="miniapp-btn">
          <i class="iconfont icon-xiaochengxu"></i>
        </div>
        <div class="miniapp-info">
          <div class="l">
            <img src="./assets/images/miniapp_try.jpg" alt="">
          </div>
          <div class="r">
            <img src="./assets/images/miniapp-info.jpg" alt="">
          </div>
          <div class="close-btn">
            <i class="iconfont icon-guanbicuowu"></i>
          </div>
        </div>
      </div>
    </div>
  </body>

</html>