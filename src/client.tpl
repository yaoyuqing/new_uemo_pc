{{include './partial/header.tpl' data={title:'客户列表',active:'client',bodyClass:'bodylist bodyclient'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pageList">
    <div id="banner" class="banner">
      <div class="bg">
        <i class="iconfont icon-baojiaquotation wow" data-wow-delay="0.2s"></i>
      </div>
      <div class="banner_info wow">
        <div class="title">我们的客户都怎么说</div>
        <div class="subtitle">80%行业佼佼者，选择魔艺建站</div>
      </div>
      <div class="num wow">
        <span class="text">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
        <span class="text back">100</span>
      </div>
    </div>
    <div id="page_content" class="page_content">
      <div class="mlist clientlist">
        <div class="module_container">
          <div class="container_content">
            <div class="content_list clearfix">
              <div class="item_block wow">
                <div class="item_info clearfix">
                  <div class="head fl">
                    <img src="./assets//images/team_img.jpg" alt="">
                  </div>
                  <div class="name fl">
                    <div class="title">聂心远</div>
                    <div class="subtitle">黑金经纪创始人兼董事长</div>
                  </div>
                </div>
                <div class="item_des">
                  <div class="description">
                    <p>
                      创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                    </p>
                    <br/>
                    <p>
                      感谢UEMO与黑金经纪一路相伴，以现代的设计理念与完善的技术服务支持，让我们看到互联网世界中的最美黑金！
                    </p>
                  </div>
                </div>
                <div class="item_link">
                  <a href="http://www.ecoux.cn/" class="link_box">
                    <span class="text">http://www.ecoux.cn/</span>
                  <i class="iconfont icon-youjiantou"></i>
                  </a>
                </div>
                <div class="autograph">
                  <img src="./assets/images/team_autograph_03.jpg" alt="">
                </div>
              </div>
              <div class="item_block wow">
                <div class="item_info clearfix">
                  <div class="head fl">
                    <img src="./assets//images/team_img.jpg" alt="">
                  </div>
                  <div class="name fl">
                    <div class="title">聂心远</div>
                    <div class="subtitle">黑金经纪创始人兼董事长</div>
                  </div>
                </div>
                <div class="item_des">
                  <div class="description">
                    <p>
                      UEmo给设计从业者提供了很好的平台，设计师不仅能从中获取到最新鲜的设计资源，还能通过平台展示自己，让广大设计师凝聚起来，一起推动中国设计前行！
                    </p>
                  </div>
                </div>
                <div class="item_link">
                  <a href="http://www.ecoux.cn/" class="link_box">
                    <span class="text">http://www.ecoux.cn/</span>
                  <i class="iconfont icon-youjiantou"></i>
                  </a>
                </div>
                <div class="autograph">
                  <img src="./assets/images/team_autograph_03.jpg" alt="">
                </div>
              </div>
              <div class="item_block wow">
                <div class="item_info clearfix">
                  <div class="head fl">
                    <img src="./assets//images/team_img.jpg" alt="">
                  </div>
                  <div class="name fl">
                    <div class="title">聂心远</div>
                    <div class="subtitle">黑金经纪创始人兼董事长</div>
                  </div>
                </div>
                <div class="item_des">
                  <div class="description">
                    <p>
                      UEmo给设计从业者提供了很好的平台，设计师不仅能从中获取到最新鲜的设计资源，还能通过平台展示自己，让广大设计师凝聚起来，一起推动中国设计前行！
                    </p>
                  </div>
                </div>
                <div class="item_link">
                  <a href="http://www.ecoux.cn/" class="link_box">
                    <span class="text">http://www.ecoux.cn/</span>
                  <i class="iconfont icon-youjiantou"></i>
                  </a>
                </div>
                <div class="autograph">
                  <img src="./assets/images/team_autograph_03.jpg" alt="">
                </div>
              </div>
              <div class="item_block wow">
                <div class="item_info clearfix">
                  <div class="head fl">
                    <img src="./assets//images/team_img.jpg" alt="">
                  </div>
                  <div class="name fl">
                    <div class="title">聂心远</div>
                    <div class="subtitle">黑金经纪创始人兼董事长</div>
                  </div>
                </div>
                <div class="item_des">
                  <div class="description">
                    <p>
                      创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                    </p>
                    <br/>
                    <p>
                      感谢UEMO与黑金经纪一路相伴，以现代的设计理念与完善的技术服务支持，让我们看到互联网世界中的最美黑金！
                    </p>
                  </div>
                </div>
                <div class="item_link">
                  <a href="http://www.ecoux.cn/" class="link_box">
                    <span class="text">http://www.ecoux.cn/</span>
                  <i class="iconfont icon-youjiantou"></i>
                  </a>
                </div>
                <div class="autograph">
                  <img src="./assets/images/team_autograph_03.jpg" alt="">
                </div>
              </div>
              <div class="item_block wow">
                <div class="item_info clearfix">
                  <div class="head fl">
                    <img src="./assets//images/team_img.jpg" alt="">
                  </div>
                  <div class="name fl">
                    <div class="title">聂心远</div>
                    <div class="subtitle">黑金经纪创始人兼董事长</div>
                  </div>
                </div>
                <div class="item_des">
                  <div class="description">
                    <p>
                      UEmo给设计从业者提供了很好的平台，设计师不仅能从中获取到最新鲜的设计资源，还能通过平台展示自己，让广大设计师凝聚起来，一起推动中国设计前行！
                    </p>
                  </div>
                </div>
                <div class="item_link">
                  <a href="http://www.ecoux.cn/" class="link_box">
                    <span class="text">http://www.ecoux.cn/</span>
                  <i class="iconfont icon-youjiantou"></i>
                  </a>
                </div>
                <div class="autograph">
                  <img src="./assets/images/team_autograph_03.jpg" alt="">
                </div>
              </div>
              <div class="item_block wow">
                <div class="item_info clearfix">
                  <div class="head fl">
                    <img src="./assets//images/team_img.jpg" alt="">
                  </div>
                  <div class="name fl">
                    <div class="title">聂心远</div>
                    <div class="subtitle">黑金经纪创始人兼董事长</div>
                  </div>
                </div>
                <div class="item_des">
                  <div class="description">
                    <p>
                      创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                    </p>
                    <br/>
                    <p>
                      感谢UEMO与黑金经纪一路相伴，以现代的设计理念与完善的技术服务支持，让我们看到互联网世界中的最美黑金！
                    </p>
                  </div>
                </div>
                <div class="item_link">
                  <a href="http://www.ecoux.cn/" class="link_box">
                    <span class="text">http://www.ecoux.cn/</span>
                  <i class="iconfont icon-youjiantou"></i>
                  </a>
                </div>
                <div class="autograph">
                  <img src="./assets/images/team_autograph_03.jpg" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}