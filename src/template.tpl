{{include './partial/header.tpl' data={title:'uemo模板列表',active:'template',bodyClass:'bodylist bodytemplate'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pageList">
    <div id="page_target" class="pageTarget">
      <div class="container_category">
        <div class="category_btn">
          <a href="javascript:;" class="item_btn active">行业</a>
          <a href="javascript:;" class="item_btn">标签</a>
          <span class="move"></span>
        </div>
        <div class="category_wrapper swiper-container">
          <div class="swiper-wrapper">
            <div class="item-slide swiper-slide">
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <a href="#" class="swiper-slide active">
                    <span class="text">全部模板</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">艺术设计</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">摄影摄像</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">建筑园林</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">企业集团</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">数码家居</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">音体传播</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">休闲旅游</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">医疗教育</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">服饰礼服</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">餐饮茶酒</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">农工林牧</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">艺术设计</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">摄影摄像</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">建筑园林</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">企业集团</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">数码家居</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">音体传播</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">休闲旅游</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">医疗教育</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">服饰礼服</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">餐饮茶酒</span>
                    <span class="num">121</span>
                  </a>
                  <div class="movedot"></div>
                </div>
                <div class="swiper-scrollbar"></div>
              </div>
            </div>
            <div class="item-slide swiper-slide">
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <a href="#" class="swiper-slide">
                    <span class="text">全部模板</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">艺术设计</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">摄影摄像</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">建筑园林</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide active">
                    <span class="text">企业集团</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">数码家居</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">音体传播</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">休闲旅游</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">医疗教育</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">服饰礼服</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">餐饮茶酒</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">农工林牧</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">艺术设计</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">摄影摄像</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">建筑园林</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">企业集团</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">数码家居</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">音体传播</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">休闲旅游</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">医疗教育</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">服饰礼服</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="swiper-slide">
                    <span class="text">餐饮茶酒</span>
                    <span class="num">121</span>
                  </a>
                  <div class="movedot"></div>
                </div>
                <div class="swiper-scrollbar"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="page_content" class="page_content">
      <div class="mlist template">
        <div class="module_container">
          <div class="container_header wow">
            <p class="title">音体传播</p>
            <p class="subtitle">
              收听完整的录音室专辑、视频游戏音轨或影片的声音设计
            </p>
          </div>
          <div class="container_content">
            <div class="content_wrapper">
              {{include './partial/template_list.tpl'}}
            </div>
          </div>
          {{include './partial/pages.tpl'}}
        </div>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}