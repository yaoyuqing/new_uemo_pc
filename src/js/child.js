// wow.js
import WOW from "./common/wowjs";

// swiper.js
import Swiper from "swiper";
// @cycjimmy/swiper-animation
// import SwiperAnimation from '@cycjimmy/swiper-animation'
// swiper.animate
// import { swiperAnimateCache, swiperAnimate } from './swiper.animate1.0.3.min';

// appear.js
import "jquery-appear-original";

// malihu-custom-scrollbar-plugin
import "malihu-custom-scrollbar-plugin";

// jquery-mousewheel
import "jquery-mousewheel";

// qrcode.js
import QRCoder from "qrcoder";

// gsap
import { gsap, MotionPathPlugin } from "gsap/all";

// webgl-fluid
import WebglFluid from "./common/webgl-fluid.umd";

// lulu ui
import LightTip from "pure/js/common/ui/LightTip.js";

// Stickyfill
// import Stickyfill from 'Stickyfill'

// mojs
import mojs from "@mojs/core";

// lottie-web
import lottie from "lottie-web";

// 
import Sketch from './common/sketch';

window.gsap = gsap;

// hover-effect
var HoverEffect = require("./common/hover-effect");

// Masonry
var Masonry = require("masonry-layout");
var jQueryBridget = require("jquery-bridget");
jQueryBridget("masonry", Masonry, $);
// imagesloaded
var imagesLoaded = require("imagesloaded");
imagesLoaded.makeJQueryPlugin($);

$(function () {
  header();
  footer();
  initQrcode();
  pageIndex();
  pageTemplate();
  pageUnit();
  pageTemplatePost();
  pageTemplateCase();
  pageCustomCase();
  pageCustomCasePost();
  pageSearch();
  pageNews();
  pageCasePost();
  pageUnitPost();
  pageClient();
  pagePrice();
  pageAgent();
  initWow();
  noticePopup();
  var customCursor = new CustomCursor();
  customCursor.init();
});

var loadingDom =
  '<div class="pay_loading_dom"><div class="pay_loading_dom_wrapper"><div class="pay_loading_dom-close_btn">x</div><div class="pay_message"><div href="javascript:;" style="padding: 10px 0;" class="pay_logo"><i class="iconfont icon-uemologo-copy" style="font-size: 32px;color: #333;"></i></div><span class="text">请稍后...</span></div><div class="windows8"> <div class="wBall" id="wBall_1"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_2"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_3"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_4"> <div class="wInnerBall"></div> </div> <div class="wBall" id="wBall_5"> <div class="wInnerBall"></div> </div> </div></div></div>';

// ajax响应拦截
$.ajaxSetup({
  contentType: "application/x-www-form-urlencoded;charset=utf-8",
  complete: function (XMLHttpRequest, textStatus) {
    let result;

    if (typeof XMLHttpRequest.responseJSON === "string") {
      return XMLHttpRequest.responseJSON;
    } else if (typeof XMLHttpRequest.responseJSON !== "object") {
      new LightTip().error("<span>接口错误</span>", 2000);
      return Promise.reject(new Error("接口错误")).catch(() => {});
    } else if (XMLHttpRequest.responseJSON.code) {
      switch (String(XMLHttpRequest.responseJSON.code)) {
        case "0":
          result = Promise.resolve(XMLHttpRequest.responseJSON.data);
          break;
        case "1":
          var msg = XMLHttpRequest.responseJSON.msg;
          new LightTip().error("<span>" + msg + "</span>", 2000);
          break;
        case "997":
          $("body").addClass("openuser");
          break;
        case "998":
          result = Promise.resolve(XMLHttpRequest.responseJSON.data);
          break;
        case "999":
          window.location = XMLHttpRequest.responseJSON.msg;
          break;
        default:
          result = Promise.reject(XMLHttpRequest);
          break;
      }
      return result;
    }
  },
});

// 静态资源路径
var _static = $("#footer").data("static");

// validator
var pattern = {
  phone: /^0?(13|14|15|17|18|19)[0-9]{9}$/,
  isEmpty: /.+/,
  password: /.{6}/,
  code: /^\d{6}$/,
  email: /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
};

// 头部
function header() {
  if(!$('#header')[0]) return;
  var scOffsetTop = $("#header").outerHeight();
  if ($(document).scrollTop() > scOffsetTop) {
    $("#header").addClass("mini");
  }
  $(document).scroll(function () {
    if ($(document).scrollTop() > scOffsetTop) {
      $("#header").addClass("mini");
    } else {
      $("#header").removeClass("mini");
    }
  });

  // search
  $("#header .header_right .search_btn").on("click", function () {
    if ($("#header").hasClass("searchopen")) {
      return false;
    }
    $("#header").addClass("searchshow");
    AnimationEnd($("#header #logo"), function () {
      $("#header").removeClass("searchshow").addClass("searchopen");
      $("#header .searchview .search_bar .searchform_input")[0].focus();
    });
  });
  $("#searchview .search_close,#searchview .search_mask").on("click", close);

  $(document).on("keyup", function (ev) {
    if (ev.keyCode === 27) {
      close();
    }
  });

  search();

  function close() {
    if ($("#header").hasClass("searchopen")) {
      $("#header").removeClass("searchopen").addClass("searchhide");
      AnimationEnd($("#header .header_right .r_animate2"), function () {
        $("#header").removeClass("searchhide");
      });
    }
  }

  // subnav
  $("#nav .navitem").on('mouseenter', function () {
    if(!$(this).hasClass('subnav')) {
      $('#header .subnav_bg').removeClass('show');
    } 
  });
  $("#nav .navitem.subnav").each(function (i, el) {
    $(el).on('mouseenter', function() {
      $('#header .subnav_bg').eq(i).addClass('show').siblings().removeClass('show');
    }); 
    $('#header, #nav .navitem:not(.subnav').on('mouseleave', function() {
      $('#header .subnav_bg').eq(i).removeClass('show');
    }); 
  });

  // 下拉菜单
  $("#header .drop_title").on("click", function () {
    $("#header .drop").toggleClass("drop-show");
  });

  blankClose($("#header .drop"), function () {
    $("#header .drop").removeClass("drop-show");
  });

  var sketchData = JSON.parse(window.alert_imgs);
  var images = [];
  var links = [];

  $.each(sketchData, function(i, item) {
    images.push(item.head_img);
    links.push(item.url);
  });

  let sketch = new Sketch({
    duration: 1.5,
    debug: true,
    easing: 'easeOut',
    images: images,
    uniforms: {
      width: {value: 7, type:'f', min:0, max:10},
      scaleX: {value: 4, type:'f', min:0.1, max:60},
      scaleY: {value: 4, type:'f', min:0.1, max:60},
    },
  });

  sketch.init();
  sketch.Events.on('tick', function({ current }) {
    $('#header .subnav_bg .sub-banner .sketch .link_box').attr('href', links[current]);
  });

  // 当前页active
  var pageName = window.location.pathname.split("/")[1];
  $("#nav .nav-a").each(function (i, nav) {
    if (pageName === "") return false;
    var currentHref = $(nav).data("href");
    if (currentHref.search(pageName) !== -1) {
      $(nav).addClass("active");
    } else {
      $(nav).removeClass("active");
    }
  });

  // 判断登录状态
  $.get("/shop/api/userinfo", function (res) {
    if (res.code === 998) {
      $("#header .header_right .manage-box").remove();
      $("#header .header_right .login-btn").removeClass("hide");
    } else if (res.code === 0) {
      $("#header .header_right .login-btn").remove();
      $("#header .header_right .manage-box").removeClass("hide");
      var _data = res.data;
      _data.new_msg &&
        $("#header .header_right .notice .num")
          .removeClass("hide")
          .text(_data.new_msg);
      _data.avatar &&
        $("#header .header_right .head img").attr("src", _data.avatar);
      _data.ainfo.name &&
        $("#header .manage-cont .user_info .name").text(_data.ainfo.name);
    }
  });
}

// 搜索
function search() {
  $("#searchview").each(function (i, el) {
    // 清空搜索词
    $(".search_keywords .clear_keyword", el).on("click", function () {
      $(".search_keywords .history_word .result_words", el).html("");
    });
    // 点击搜索
    $(".searchform_submit", el).on("click", function () {
      var searchAddr = window.location.origin;
      var inpVal = $(".searchform_input", el).val();
      window.open(`${searchAddr}/search/${inpVal}`);
      return false;
    });
  });
}

// 底部
function footer() {
  if(!$('#footer')[0]) return;
  var $popupQr = $(".popup_qr");
  $(".open-popup").on("click", function () {
    var _type = ".pop-" + $(this).data("type");
    $popupQr.filter(_type).fadeIn(300, function () {
      $(".fixed-container", this).addClass("show");
    });
  });
  $(document).on("click", ".popup_qr", function (event) {
    if (event.target === this) {
      $(this).fadeOut(300, function () {
        $(".fixed-container", this).removeClass("show");
      });
    }
  });
}

// 首页
function pageIndex() {
  $(".bodyindex").each(function () {
    indexSlider();
    indexCounter();
    indexTemplate();
    indexCase();
    indexTeam();
    indexNews();
  });
}

// 首页幻灯
function indexSlider() {
  var $slider = $("#mslider .swiper-container");
  initSlider($slider, {
    loop: true,
    autoplay: {
      disableOnInteraction: false,
      delay: 3000,
    },
    navigation: {
      nextEl: "#mslider .swiper-button-next",
      prevEl: "#mslider .swiper-button-prev",
    },
  });

  $slider.on("mouseover", function () {
    $slider[0].swiper.autoplay.stop();
  });
  $slider.on("mouseout", function () {
    $slider[0].swiper.autoplay.start();
  });

  // 第一屏效果
  $(".renderSurface").each(function (i, canvas) {
    WebglFluid(canvas, {
      HOVERDOM: canvas,
      IMMEDIATE: false,
      SIM_RESOLUTION: 256,
      DYE_RESOLUTION: 1024,
      CAPTURE_RESOLUTION: 512,
      DENSITY_DISSIPATION: 3.5,
      VELOCITY_DISSIPATION: 0,
      PRESSURE: 1,
      PRESSURE_ITERATIONS: 20,
      CURL: 0,
      SPLAT_RADIUS: 0.25,
      SPLAT_FORCE: 6000,
      SHADING: true,
      COLORFUL: true,
      COLOR_UPDATE_SPEED: 10,
      PAUSED: false,
      BACK_COLOR: { r: 89, g: 66, b: 210 },
      TRANSPARENT: true,
      BLOOM: true,
      BLOOM_ITERATIONS: 8,
      BLOOM_RESOLUTION: 256,
      BLOOM_INTENSITY: 0.8,
      BLOOM_THRESHOLD: 0.6,
      BLOOM_SOFT_KNEE: 0.7,
      SUNRAYS: true,
      SUNRAYS_RESOLUTION: 196,
      SUNRAYS_WEIGHT: 1.0,
    });
  });
}

// 首页数字滚动
function indexCounter() {
  $("#mcounter .content_list li .number").each(function (i, el) {
    var number = $(el).data("counter-value");
    var numArr = ("" + number).split("");
    var str = "";
    for (let i = 0; i < numArr.length; i++) {
      str += `<span class="counterDX wow">${numArr[i]}</span>`;
    }
    $(".unit", el).before(str);
    $(".counterDX", el).each(function (j, n) {
      $(n).attr("data-wow-delay", `${(j * 0.12).toFixed(1)}s`);
    });
  });
}

// 首页产品
function indexTemplate() {
  $("#template").each(function (i, el) {
    $(".container_category .header_btn .choice", el).click(function () {
      $(".container_category .category_menu", el).addClass("show");
    });

    blankClose($(".container_category", el), function () {
      $(".container_category .category_menu", el).removeClass("show");
    });

    // 随机按钮
    $(".header_btn .random", el).on("click", function () {
      var fileUrl = $(".container_content", el).data("files");
      $.ajax({
        type: "GET",
        async: true,
        url: "/shop/openapi/product_rand",
        success: function (res) {
          if (res.code === 0) {
            var data = res.data;
            var templateList = "";
            $.each(data, function (index, item) {
              templateList +=
                '<div class="item_block  wow" data-wow-delay="0.' +
                (index % 3) +
                's"><div class="item_img"><a href="' +
                item.url +
                '" class="item_box"><img src="' +
                fileUrl +
                item.img +
                '" alt=""></a><div class="pro-mask"><a href="' +
                item.pre_url +
                '" class="preview" target="_blank">点击预览</a><a href="' +
                item.url +
                '" class="pro-details" target="_blank">免费试用</a></div></div><div class="item_wrapper"><div class="item_info"><p class="title ellipsis">' +
                item.title +
                '</p><p class="subtitle"><span class="num">编号</span><span class="id">' +
                item.username +
                "</span></p></div></div></div>";
            });

            $(".content_list", el).html(templateList);
          }
        },
      });
    });
  });
}

// 首页案例
function indexCase() {
  $("#case").each(function (i, el) {
    // 分类
    var $cateSlider = $(".container_category .category_wrapper", el);
    var curentIndex = $(".swiper-slide.active", $cateSlider).index();

    initSlider($cateSlider, {
      initialSlide: curentIndex,
      loop: true,
      slidesPerView: "auto",
      loopedSlides: 50,
      centeredSlides: true,
      slideToClickedSlide: true,
      watchSlidesProgress: true,
      watchSlidesVisibility: true,
      on: {
        slideChangeTransitionStart: function () {
          $(".swiper-slide", $cateSlider).removeClass("start-mask end-mask");
        },
        slideChangeTransitionEnd: function () {
          var _width = 0;
          var maskIndex;
          var slideW = $(".swiper-slide-visible", $cateSlider).outerWidth(true);
          $(".swiper-slide-visible", $cateSlider).each(function () {
            _width += $(this).outerWidth(true);
          });

          if (Math.abs(this.width - _width) > slideW) {
            maskIndex = Math.floor(
              ($(".swiper-slide-visible", $cateSlider).length - 2) / 2 - 1
            );
          } else {
            maskIndex = Math.floor(
              $(".swiper-slide-visible", $cateSlider).length / 2 - 1
            );
          }
          $(".swiper-slide-active", $cateSlider)
            .prevAll()
            .eq(maskIndex)
            .addClass("start-mask");
          $(".swiper-slide-active", $cateSlider)
            .nextAll()
            .eq(maskIndex)
            .addClass("end-mask");
          this.slides.removeClass("active");
          this.slides.eq(this.activeIndex).addClass("active");
        },
      },
      navigation: {
        nextEl: "#case .container_category .swiper-button-next",
        prevEl: "#case .container_category .swiper-button-prev",
      },
    });

    $(".swiper-button", el).on("click", function () {
      $(".swiper-slide-active", $cateSlider).trigger("click");
    });

    // 用户
    initUser(el);
    function initUser($el) {
      var $userSliderInfo = $(
        ".container_content .content_info .content_wrapper",
        $el
      );
      var swiperInfo = initSlider($userSliderInfo, {
        allowTouchMove: false,
        effect: "fade",
        fadeEffect: {
          crossFade: true,
        },
      });

      $(".item_index .current_num", $userSliderInfo).each(function () {
        $(this).text(zero($(this).text(), 2));
      });

      var $userSliderImage = $(
        ".container_content .content_images .content_wrapper",
        $el
      );
      initSlider($userSliderImage, {
        loop: $(".item_block", $userSliderImage).length > 1,
        slidesPerView: "auto",
        loopedSlides: 3,
        spaceBetween: "14%",
        centeredSlides: true,
        slideToClickedSlide: true,
        navigation: {
          nextEl: "#case .content_images .swiper-button-next",
          prevEl: "#case .content_images .swiper-button-prev",
        },
        pagination: {
          el: "#case .content_images .swiper-pagination",
          type: "fraction",
        },
        on: {
          slideChangeTransitionStart: function () {
            swiperInfo.slideTo(this.realIndex);
          },
        },
      });

      $(".content_images .item_block", el).hover(
        function () {
          $(".content_info", el).addClass("visible");
        },
        function () {
          $(".content_info", el).removeClass("visible");
        }
      );
    }

    // ajax切换
    $(".container_category a", el).on("click", function () {
      if ($(this).hasClass("active")) return false;

      var $caseEl = $(
        '<div class="container_content wow"><div class="content_images"><div class="content_wrapper swiper-container"><div class="content_list swiper-wrapper"></div><div class="button-wrapper prev"><div class="swiper-button-prev"></div></div><div class="button-wrapper next"><div class="swiper-button-next"></div></div><div class="swiper-pagination"></div></div></div><div class="content_info"><div class="content_wrapper swiper-container"><div class="content_list swiper-wrapper"></div></div></div></div>'
      );

      var newparam = $(this).data("cats");
      var fileUrl = $("#template .container_content").data("files");
      var originUrl = window.location.origin;

      $.ajax({
        url: "/shop/openapi/get_cat_info",
        data: {
          name: newparam,
        },
        success: function (res) {
          if (res.code === 0) {
            var data = res.data.sites;
            var imgStr = "";
            var infoStr = "";

            $.each(data, function (index, item) {
              let _title = item.title ? item.title : "";
              let _subTitle = item.sub_title ? item.sub_title : "";

              imgStr +=
                '<div class="item_block swiper-slide"><a href="' +
                item.pre_url +
                '" class="item_box"><div class="item_img"><img src="' +
                fileUrl +
                item.img +
                '" alt=""/></div></a><div class="pro-mask"><a href="' +
                item.pre_url +
                '" class="preview" target="_blank">浏览网站</a></div><div class="qrcode_btn"><div class="icon"><i class="iconfont icon-erweima"></i></div><div class="qrcode_wrapper" data-src="' +
                originUrl +
                item.pre_url +
                '"><img src="" alt="" class="qrcode"/><span class="text">请使用微信扫一扫查看</span></div></div></div>';

              infoStr +=
                '<div class="item_block swiper-slide"><div class="item_info"><p class="title ellipsis"><span class="text">' +
                _title +
                '</span></p><p class="subtitle"><span class="text">' +
                _subTitle +
                '</span></p></div><div class="item_index"><span class="current_num">' +
                (index + 1) +
                "</span></div></div>";
            });
          }

          $(".content_images .content_list", $caseEl).html(imgStr);
          $(".content_info .content_list", $caseEl).html(infoStr);
          $("#case .container_content").replaceWith($caseEl);
          initUser(el);
          initQrcode();
        },
      });
      return false;
    });
  });
}

// 首页团队
function indexTeam() {
  var $slider = $("#client .swiper-container");
  initSlider($slider, {
    loop: true,
    slidesPerView: 2,
    centeredSlides: true,
    spaceBetween: "5.3%",
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  });
  initCustomScrollbar();
}

// 首页新闻
function indexNews() {
  var swiperWidthFix = Math.floor($("#news .module_container").width());
  $("#news .module_container .swiper-container").css({ width: swiperWidthFix });
  var $slider = $("#news .swiper-container");
  var $newsCategory = $("#news .container_category");
  initSlider($slider, {
    slidesPerView: "auto",
    allowTouchMove: false,
    spaceBetween: 94,
    autoHeight: true,
    observer: true,
    observeParents: true,
    pagination: {
      el: ".news-swiper-pagination",
      renderBullet: function (index, className) {
        return `<span class="${className}">${$("a", $newsCategory)
          .eq(index)
          .text()}</span>`;
      },
      clickable: true,
    },
    on: {
      resize: function () {
        var resizeW = Math.floor($("#news .container_content").outerWidth());
        $("#news .container_content .swiper-container").width(resizeW);
      },
    },
  });
}

// 模板列表页
function pageTemplate() {
  $(".bodytemplate").each(function (i, el) {
    // 分类
    var $cateSlider = $(".container_category .category_wrapper", el);
    var $itemSlide = $(".item-slide", $cateSlider);
    var $cateBtn = $(".category_btn .item_btn", el);
    var $move = $(".category_btn span.move", el);
    var curentIndex = $cateBtn.filter(".active").index();
    btnMove(curentIndex);

    var sliderCate = initSlider($cateSlider, {
      initialSlide: curentIndex,
      allowTouchMove: false,
      slidesPerView: 1,
      speed: 300,
      observer: true,
      preventClicks: false,
      observeParents: true,
    });

    $itemSlide.each(function (index, item) {
      var $movedot = $(".movedot", item);

      initSlider($(".swiper-container", item), {
        slidesPerView: "auto",
        resistanceRatio: 0,
        observer: true,
        observeParents: true,
        mousewheel: {
          sensitivity: 1200,
          releaseOnEdges: true,
        },
        speed: 90,
        direction: "vertical",
        scrollbar: {
          el:
            ".item-slide:nth-child(" +
            (index + 1) +
            ") .swiper-container .swiper-scrollbar",
          draggable: true,
        },
        on: {
          init: function () {
            var _top =
              $(".active", item).offset().top - $cateSlider.offset().top + 14;
            gsap.set($movedot, {
              y: _top,
            });
          },
        },
      });

      $(item).on("mouseover", ".swiper-slide", function () {
        var _top = $(this).position().top + 14;
        gsap.to($movedot, {
          y: _top,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      });
      $(item).on("mouseleave", function () {
        var _top = $(".swiper-slide.active", item).position().top + 14;
        gsap.to($movedot, {
          y: _top,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      });
    });

    $cateBtn.on("click", function () {
      var index = $(this).index();
      $(this).addClass("active").siblings().removeClass("active");
      btnMove(index);
      sliderCate.slideTo(index);
    });

    function btnMove(_cur) {
      if (_cur > 0) {
        $move.css({
          left: "50%",
        });
      } else {
        $move.css({
          left: 0,
        });
      }
    }

    $(".item_block .preview", el).on("click", function () {
      var href = $(this).attr("href");
      var pathname = window.location.pathname;
      window.open(href + "?" + pathname);
      return false;
    });

    pin(
      {
        containerSelector: ".bodytemplate .pageList",
        activeClass: "fixed",
        padding: {
          top: 80,
        },
      },
      $(".pageTarget .container_category", el)
    );
  });
}

// 组件列表页
function pageUnit() {
  $(".bodyunit").each(function (i, el) {
    // 分类
    var $cateSlider = $(".container_category .category_wrapper", el);
    var $cateBtn = $(".category_btn .item_btn", el);
    var $move = $(".category_btn span.move", el);
    var curentIndex = $cateBtn.filter(".active").index();
    var $movedot = $(".movedot", $cateSlider);
    btnMove(curentIndex);

    $cateBtn.on("click", function () {
      var index = $(this).index();
      $(this).addClass("active").siblings().removeClass("active");
      btnMove(index);
    });

    function btnMove(_cur) {
      if (_cur > 0) {
        $move.css({
          left: "50%",
        });
      } else {
        $move.css({
          left: 0,
        });
      }
    }

    var _top = $("a.active", $cateSlider).offset().top - $cateSlider.offset().top + 14;
    console.log(_top);
    gsap.set($movedot, {
      y: _top,
    });

    $("a", $cateSlider).each(function (index, item) {
      $(item).on("mouseover", function () {
        var _top = $(this).position().top + 14;
        gsap.to($movedot, {
          y: _top,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      });
      $(item).on("mouseleave", function () {
        gsap.to($movedot, {
          y: _top,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      });
    });

    pin(
      {
        containerSelector: ".bodyunit .pageList",
        activeClass: "fixed",
        padding: {
          top: 80,
        },
      },
      $(".pageTarget .container_category", el)
    );
  });
}

// 模板详情页
function pageTemplatePost() {
  $(".bodytemplatepost").each(function (i, el) {
    // 客户轮播
    var $customerSlider = $(".customer .swiper-container", el);
    initSlider($customerSlider, {
      scrollbar: {
        el: ".swiper-scrollbar",
        draggable: true,
      },
      slidesPerView: "auto",
      spaceBetween: 50,
    });

    // 查看更多
    var $info = $(".post_introduction .intr_info .abstract .des_wrap", el);
    $(".post_introduction .intr_info .more_btn", el).click(function () {
      $info.addClass("show");
      $(".description", $info).mCustomScrollbar();
    });

    blankClose($(".post_introduction .intr_info", el), function () {
      upDes();
    });

    $(".up_btn", $info).click(function () {
      upDes();
    });

    function upDes() {
      $info.removeClass("show");
      $(".description", $info).mCustomScrollbar("destroy");
    }

    // 相关tab切换
    var swiperWidthFix = Math.floor($(".relevant .module_container").width());
    $(".relevant .module_container .swiper-container").css({
      width: swiperWidthFix,
    });
    var $relevantSlider = $(".relevant .swiper-container", el);
    var $relevantCategory = $(".relevant .post-swiper-pagination", el);
    initSlider($relevantSlider, {
      slidesPerView: "auto",
      allowTouchMove: false,
      spaceBetween: 100,
      autoHeight: true,
      observer: true,
      observeParents: true,
      pagination: {
        el: ".post-swiper-pagination",
        renderBullet: function (index, className) {
          return `<span class="${className}">${$(
            $("a", $relevantCategory).eq(index)
          ).text()}</span>`;
        },
        clickable: true,
      },
      on: {
        resize: function () {
          var resizeW = Math.floor(
            $(".relevant .container_content").outerWidth()
          );
          $(".relevant .container_content .swiper-container").width(resizeW);
        },
      },
    });
    // 移动到更多按钮下
    if ($("span", $relevantCategory).length > 6) {
      var $moreBullet = $(`<div class="more_btn">
          <div class="title">
              <span>更多</span>
              <i class="iconfont icon-jiantou5-copy-copy-copy"></i>
          </div>
          <div class="tags-list"></div>
          </div>`);
      $(".swiper-pagination-bullet", $relevantCategory).each(function (n, a) {
        if (n < 6) {
          $(".tags-list", $moreBullet).append(`<span class="hide"></span>`);
        } else {
          $(".tags-list", $moreBullet).append($(a));
        }
      });
      $relevantCategory.append($moreBullet);

      $(".more_btn", $relevantCategory).hover(
        function () {
          $(".tags-list", this).stop(true).slideDown();
          $(this).addClass("active");
        },
        function () {
          $(".tags-list", this).stop(true).slideUp();
          $(this).removeClass("active");
        }
      );
    }

    // 判断是否登录
    $(".confirm_btn", el).on("click", function (e) {
      e.preventDefault();
      $(loadingDom).appendTo("body");

      var siteId = $(this).data("id");
      $.ajax({
        type: "GET",
        async: true,
        url: "/shop/api/to_try",
        data: {
          site_id: siteId,
        },
        dataType: "json",
        beforeSend: function () {
          $(loadingDom).appendTo("body");
        },
        success: function (res) {
          if (res.code === 998) {
            window.location = res.msg;
          } else if (res.code === 997) {
            initUserInfo(initInput);
            $(".pay_loading_dom").remove();
          } else {
            $(".pay_loading_dom").remove();
          }
        },
        error: function () {
          $(".pay_loading_dom").remove();
        },
      });
    });

    // 基本信息补全
    var getuserinfo = {};
    var userinfo = {
      company_name: "",
      email: "",
      qq: "",
    };
    var dataFormState = {
      company_name: -1,
      email: -1,
      qq: -1,
    };

    function initUserInfo(callback) {
      $.get("/shop/api/userinfo", function (data) {
        getuserinfo = data.data;
        userinfo = {
          company_name: getuserinfo.ainfo.name,
          contacts: getuserinfo.ainfo.contacter,
          email: getuserinfo.email,
          qq: getuserinfo.ainfo.qq,
        };
        callback && callback();
      });
    }

    function initInput() {
      $(".user-setting .custom_el_input").each(function (i, elm) {
        var inputName = $("input", elm).attr("name");
        var isRequired = $(elm).hasClass("custom_el_input_required");
        var _pattern = pattern[inputName];
        var errMsgEl =
          $(".err_msg", elm).text().length > 2 && $(".err_msg", elm);
        $("input", elm).val(userinfo[inputName]);
        validator($("input", elm));

        $(elm)
          .off("focusin")
          .on("focusin", function () {
            $(this).addClass("state_focus");
          });
        $(elm)
          .off("focusout")
          .on("focusout", function () {
            $(this).removeClass("state_focus");
          });
        $("input", elm)
          .off("input")
          .on("input", function () {
            var _val = $(this).val().trim();
            userinfo[inputName] = _val;
            var notEmpty = pattern.isEmpty.test(_val);
            var _isErr = $(this).closest(".state_error")[0];
            if (isRequired && !notEmpty) {
              $(this)
                .closest(".custom_el_input")
                .removeClass("not_empty")
                .addClass("state_error");
              errMsgEl && errMsgEl.show(0);
            } else if (_isErr) {
              validator($(this));
            } else {
              $(this)
                .closest(".custom_el_input")
                .addClass("not_empty")
                .removeClass("state_error");
              errMsgEl && errMsgEl.hide(0);
            }
            validator($(this));
          });
        $("input", elm)
          .off("blur")
          .on("blur", function () {
            validator($(this));
          });

        function validator($input) {
          var _val = $input.val().trim();
          var $customInp = $input.closest(".custom_el_input");
          var notEmpty = pattern.isEmpty.test(_val);
          var isSuc =
            notEmpty && _pattern
              ? _pattern.test(_val)
              : pattern.isEmpty.test(_val);

          if (notEmpty) {
            $customInp.addClass("not_empty");
          } else {
            $customInp.removeClass("not_empty");
          }
          if (isRequired) {
            if (notEmpty && isSuc) {
              $customInp.removeClass("state_error");
              errMsgEl && errMsgEl.hide(0);
            } else {
              $customInp.addClass("state_error");
              errMsgEl && errMsgEl.show(0);
            }
          } else {
            if (notEmpty) {
              if (isSuc) {
                $customInp.removeClass("state_error");
                errMsgEl && errMsgEl.hide(0);
              } else {
                $customInp.addClass("state_error");
                errMsgEl && errMsgEl.show(0);
              }
            } else {
              $customInp.removeClass("state_error");
              errMsgEl && errMsgEl.hide(0);
            }
          }

          dataFormState[$input.attr("name")] = !$customInp.hasClass(
            "state_error"
          )
            ? 1
            : -1;
          getUserFormIsChange();
        }

        function getUserFormIsChange() {
          let originValue = {
            company_name: getuserinfo.ainfo.name,
            contacts: getuserinfo.ainfo.contacter,
            email: getuserinfo.email,
            qq: getuserinfo.ainfo.qq,
          };
          let isChange = Object.entries(userinfo).some(([key, value]) => {
            return value !== originValue[key];
          });

          let isValidate = Object.values(dataFormState).every(
            (item) => item > 0
          );

          $(".user-setting .submit_btn", el).toggleClass(
            "state-disable",
            !(isValidate && isChange)
          );
        }
      });

      $(".user-setting .submit_btn", el)
        .off()
        .on("click", function () {
          if ($(this).hasClass("state-disable")) return false;
          $.post(
            "/shop/api/user_update",
            {
              name: userinfo.company_name,
              qq: userinfo.qq,
              contacter: userinfo.contacts,
              email: userinfo.email,
            },
            function () {
              initUserInfo(initInput);
              $("body").removeClass("openuser");
              $(".confirm_btn", el).trigger("click");
            }
          );
        });
    }
  });
}

// 模板案例列表页
function pageTemplateCase() {
  $(".bodycase").each(function (i, el) {
    // 分类
    var $cateSlider = $(".container_category .category_wrapper", el);
    var curentIndex = $(".swiper-slide.active", $cateSlider).index();

    initSlider($cateSlider, {
      initialSlide: curentIndex,
      slidesPerView: "auto",
      spaceBetween: "0.7%",
      slideToClickedSlide: true,
      watchSlidesProgress: true,
      watchSlidesVisibility: true,
      navigation: {
        nextEl: $(".user .swiper-button-next", el),
        prevEl: $(".user .swiper-button-prev", el),
      },
    });

    // miniapp code
    var dataList = [];
    $(".miniapp")
      .next()
      .each(function (index, item) {
        var url = $(item).data("href");
        var id = $(item).data("id");

        if (checkURL(url)) {
          dataList.push({
            url,
            cardid: id,
          });
        }
      });

    if (dataList.length) {
      $.ajax({
        type: "POST",
        url: "https://card.uemox.com/Api/Site/GetMobanQrcodeList",
        data: JSON.stringify(dataList),
        success: function (res) {
          if (res.IsSuccess) {
            $(".miniapp")
              .next()
              .each(function (index, item) {
                $("img", item).attr("src", res.Data[index].qrcode);
              });
          }
        },
      });
    }

    $(".user .item_block .item_img").each(function (index, itemImg) {
      /* eslint-disable */
      new HoverEffect({
        parent: $(itemImg)[0],
        intensity: "-0.65",
        speedIn: "1.2",
        speedOut: "1.2",
        easing: undefined,
        hover: undefined,
        image1: $(itemImg).data("src"),
        image2: $(itemImg).data("src"),
        displacementImage: _static + 'assets/staticfile/images/4.png',
      });
      /* eslint-enable */
    });
  });
}

// 定制列表页
function pageCustomCase() {
  $(".bodycustomcase").each(function (i, el) {
    // 下拉菜单位置
    $(".customcase .container_category .title .iconfont", el).on(
      "click",
      function () {
        var $padding = parseFloat($(this).parent().css("padding-top"));
        var $height = $(this).parent().outerHeight();
        var $offset = $(
          ".page_content .customcase .container_category .title .iconfont",
          el
        ).offset();
        var $center = $(window).height() / 2;
        var pos = {
          x: $offset.left,
          y: $offset.top - $(document).scrollTop(),
        };

        var dir = $center > pos.y;

        $(el).addClass("category_show");
        $(".category-list", el).css({
          top: pos.y + (dir ? $padding : -$height),
          left: pos.x,
        });
      }
    );

    $(".overlay", el).on("click", function (e) {
      if ($(el).hasClass("category_show")) {
        $(el).removeClass("category_show");
        $(".category-list", el).css("top", "120%");
      }
    });
    $(".category-list", el).on("click", function (e) {
      e.stopPropagation();
    });

    // scrollTop到案例
    var _height = Math.ceil($(".banner", el).height()) - 60;
    var page = window.location.search.split("page=")[1];
    var pathname = window.location.pathname.split("/")[2];
    if (page || pathname) {
      $(document).scrollTop(_height);
    }

    // smile
    var smileAnime = lottie.loadAnimation({
      container: document.querySelector(".bodycustomcase .banner_info .smile"),
      renderer: "svg",
      loop: false,
      autoplay: false,
      path: _static + "assets/staticfile/json/smile.json",
      rendererSettings: {
        viewBoxSize: "100 10 150 220",
      },
    });

    AnimationEnd($(".info_left", el), handleAnime);

    $(".smile", el).on("mouseenter", handleAnime);

    function handleAnime() {
      if (smileAnime.isPaused) {
        smileAnime.goToAndPlay(0);
      }
    }
  });
}

// 定制案例详情页
function pageCustomCasePost() {
  $(".bodycustomcasepost").each(function (i, el) {
    var $footer = $(".postPage_footer", el);
    var footerH = $footer.outerHeight();
    if ($footer[0]) {
      footerScroll();
      $(document).on("scroll", footerScroll);
    }
    function footerScroll() {
      var footerTop = $footer.offset().top - $(window).height();

      if ($(document).scrollTop() > footerTop) {
        var scrollHeight = $(document).height() - $(window).height();
        var rule = footerH / scrollHeight;
        var _height = (scrollHeight - $(document).scrollTop()) * rule * 1.2;
        var _percent =
          (($(document).scrollTop() - footerTop) / footerH > 1
            ? 1
            : ($(document).scrollTop() - footerTop) / footerH) *
            100 +
          "%";

        gsap.set($(".container", $footer), {
          y: _percent,
        });
        gsap.to($(".next_bg", $footer), {
          scale: _height / 1000 + 1,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
        gsap.to($(".next_btn", $footer), {
          y: -_height,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      }
    }
  });
}

// 模板案例详情页
function pageCasePost() {
  $(".uemo").each(function (i, el) {
    // 返回按钮
    var $back = $("#control .control-menu .back", el);
    var $iFrame = $("#contentFrame");
    var $mask = $("#mask", el);
    $mask.hide();

    $back.click(function () {
      window.location.href = window.location.origin;
    });

    window.history.pushState(null, null, document.URL);
    window.addEventListener("popstate", function () {
      var isPreviewUrl = document.referrer.indexOf("preview") > 0;
      var prevUrl = $(".try_btn.prev a").attr("href");
      var linkOrigin = window.location.origin;
      if (isPreviewUrl) {
        var newStatus = {
          url: linkOrigin + prevUrl,
        };
        window.history.pushState(newStatus, null, document.URL);
        window.location.href = newStatus.url;
      }
    });

    // miniapp
    var templateUrl = $("#control .control-menu .url a", el).attr("href");

    if (checkURL(templateUrl)) {
      $(".miniapp-popup", el).css({
        display: "flex",
      });
      $.ajax({
        type: "POST",
        url: "https://card.uemox.com/Api/Site/GetMobanQrcode",
        data: JSON.stringify({
          url: templateUrl,
          cardid: "364",
        }),
        success: function (res) {
          if (res.IsSuccess) {
            $(".miniapp-popup .miniapp-info .l img", el).attr(
              "src",
              res.Data.qrcode
            );
            $(".miniapp-info", el).fadeIn();
          }
        },
      });

      $(".close-btn", el).on("click", function () {
        $(".miniapp-info", el).fadeOut();
      });
      $(".miniapp-btn", el).on("click", function () {
        $(".miniapp-info", el).fadeIn();
      });
    }

    
    // pc/mobile切换
    var _switch = false;
    var switchFun = function (btnStr) {
      $(".control-menu .device-select", el)
        .find(btnStr)
        .click(function () {
          var _search = $iFrame.data("src").indexOf("?") > 0;
          var _sw = btnStr.substr(1) === "desktop";
          if (_switch) {
            return false;
          }
          _switch = true;
          $iFrame.html("");
          $mask.fadeIn(0);

          $iFrame
            .attr(
              "src",
              _sw
                ? $iFrame.data("src")
                : _search
                ? `${$iFrame.data("src")}&m=1`
                : `${$iFrame.data("src")}?m=1`
            )
            .on("load", function () {
              $mask.fadeOut("slow");
              _switch = false;
            });
          $(".device-select a", el).removeClass("active");
          $(this).addClass("active");
          $("#contentArea").attr("class", "").addClass(btnStr.substr(1));
          if(_sw && checkURL(templateUrl)) {
            $(".miniapp-popup", el).fadeIn();
          } else {
            $(".miniapp-popup", el).fadeOut();
          }
        });
    };
    switchFun(".desktop");
    switchFun(".mobile");
  });
}

// 组件展示页
function pageUnitPost() {
  $(".bodyunitpost").each(function (i, el) {
    var $iFrame = $("#contentFrame");
    var $mask = $("#mask", el);
    $mask.hide();

    // pc/mobile切换
    var _switch = false;
    var switchFun = function (btnStr) {
      $(".device-select", el)
        .find(btnStr)
        .click(function () {
          var _search = $iFrame.data("src").indexOf("?") > 0;
          if (_switch) {
            return false;
          }
          _switch = true;
          $iFrame.html("");
          $mask.fadeIn(0);
          $iFrame
            .attr(
              "src",
              btnStr.substr(1) === "desktop"
                ? $iFrame.data("src")
                : _search
                ? `${$iFrame.data("src")}&m=1`
                : `${$iFrame.data("src")}?m=1`
            )
            .on("load", function () {
              $mask.fadeOut("slow");
              _switch = false;
            });
          $(".device-select a", el).removeClass("active");
          $(this).addClass("active");
          $("#contentArea").attr("class", "").addClass(btnStr.substr(1));
        });
    };
    switchFun(".desktop");
    switchFun(".mobile");

    $('.browse-btn', el).on('click',function() {
      var _preview = $('.preview', el);
      _preview.toggleClass('large');
      var $headerH = $('#header').outerHeight();
      
      var anime = gsap.fromTo(_preview,{
        width: '74%',
        height: '97%',
        y: 0,
      }, {
        
        width: '100%',
        height: `calc(100vh - ${($headerH*0.8)/2}px)`,
        y: -$headerH*0.8,
        ease: "Power1.easeOut",
        duration: 0.56,
      });

      if(_preview.hasClass('large')) {
        anime.play().eventCallback("onStart", function() {
              $('.sitecontent', el).css({
                'z-index': '10000'
              });
        });
      } else {
        anime.reverse(0).eventCallback("onReverseComplete", function() {
          $('.sitecontent', el).css({
            'z-index': '2'
          });
        });
      }
    });

    $(window).on('resize', function() {
      var _preview = $('.preview', el);
      var $headerH = $('#header').outerHeight();

      if(_preview.hasClass('large')) {
        gsap.set(_preview, {
          height: `calc(100vh - ${($headerH*0.8)/2}px)`,
          y: -$headerH*0.8,
        });
      }
    });

    // 预览图
    var $pageSW = $('.control .page-switch', el);
    var $pageSWSlider = $(".swiper-container", $pageSW);
    var $slideLen = $('.swiper-slide', $pageSWSlider).length;
    
    $(".page-switch .swiper-button", el).on('mouseenter', function() {
      $pageSW.addClass('show');
    });
    $(".page-switch .swiper-button", el).on('mouseleave', function() {
      $pageSW.removeClass('show');
    });

    if($slideLen > 1) {
      initSlider($pageSWSlider, {
        slidesPerView: 1,
        on: {
          init: function () {
            var _that = this;
            $(".page-switch .swiper-button", el).on('mouseenter', function() {
              if($(this).index() > 0) {
                _that.slideNext();
              } else {
                _that.slidePrev();
              }
            });
          },
        }
      });
    }
  });
}

// 客户列表页
function pageClient() {
  $(".bodyclient").each(function (i, el) {
    // 数字入场
    AnimationEnd($(".banner .num", el), function () {
      $(this).addClass("show");
    });

    // 瀑布流
    $(window).on("load", function () {
      var $grid = $(".clientlist .content_list", el).masonry({
        horizontalOrder: true,
        itemSelector: ".item_block",
      });
      $grid.imagesLoaded().progress(function () {
        $grid.masonry();
      });
    });
  });
}

// 价格页面
function pagePrice() {
  $(".bodyprice").each(function (i, el) {
    pin(
      {
        containerSelector: ".bodyprice .pricelist",
        activeClass: "fixed",
        padding: {
          top: 60,
          bottom: 60,
        },
        distance: {
          bottom: 180,
        },
      },
      $(".pricelist .container_header", el)
    );

    // 点击加载更多
    $(".pricelist .rights .more", el).on("click", function () {
      $(".pricelist", el).addClass("show");
      $(".pricelist .rights .item_block", el).show();
      $(this).hide();
    });

    // 选择区域
    $(
      ".pricelist .rights .item_option li, .pricelist .pro_type .item_type",
      el
    ).on("mouseover", function () {
      var index = $(this).index();
      $(".pricelist .pro_type .item_type", el)
        .eq(index)
        .addClass("active")
        .siblings()
        .removeClass("active");
      $(".pricelist .rights .column .item_col", el)
        .eq(index)
        .addClass("active")
        .siblings()
        .removeClass("active");
    });
  });
}

// 代理页面
function pageAgent() {
  $(".bodyagent").each(function (i, el) {
    // 顶部效果
    gsap.registerPlugin(MotionPathPlugin);
    gsap.set(".m1_stage", { x: "60%" });
    gsap
      .timeline({ defaults: { duration: 45 } })
      .from(".main1", { duration: 1, autoAlpha: 0, ease: "power1.inOut" }, 0)
      .fromTo(
        ".m1_cGroup",
        { opacity: 0 },
        { duration: 0.3, opacity: 1, stagger: -0.1 },
        0
      )
      .from(
        ".m1_cGroup",
        {
          duration: 2.5,
          scale: -0.3,
          transformOrigin: "50% 50%",
          stagger: -0.05,
          ease: "elastic",
        },
        0
      )
      .fromTo(
        ".m1Bg",
        { opacity: 0 },
        { duration: 1, opacity: 1, ease: "power2.inOut" },
        0.2
      )

      .add("orbs", 1.2)
      .add(function () {
        $(".banner").on("mousemove", function (e) {
          gsap.to(".m1_cGroup", {
            duration: 1,
            x: function (i) {
              return (
                ((e.clientX - window.innerWidth / 2) /
                  window.innerWidth /
                  (i + 1)) *
                150
              );
            },
            y: function (i) {
              return i * -20 * (e.clientY / window.innerHeight);
            },
            rotation: Math.random() * 0.1,
            overwrite: "auto",
          });
          gsap.to(".c1_solid, .c1_line", {
            duration: 1,
            attr: { opacity: 1.1 - 0.75 * (e.clientY / window.innerHeight) },
          });
        });

        $(".main1").on("click", function (e) {
          if (gsap.getProperty(".m1_cGroup", "scale") !== 1) return; // prevent overlapping bouncy tweens
          for (var i = 0; i < $(".m1_cGroup").length; i++) {
            gsap.fromTo(
              $(".m1_cGroup")[i],
              { transformOrigin: "50% 50%", scale: 1 },
              {
                duration: 0.7 - i / 25,
                scale: 0.9,
                ease: "back.in(10)",
                yoyo: true,
                repeat: 1,
              }
            );
          }
        });
      }, "orbs+=0.5")

      .fromTo(
        ".orb1",
        { xPercent: -35, yPercent: -5 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line1", false)[0];
            },
            start: 1.03,
            end: 1.22,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".orb2",
        { xPercent: -45, yPercent: -10 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line2", false)[0];
            },
            start: 0.98,
            end: 1.2,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".orb2b",
        { xPercent: -45, yPercent: -10 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line2", false)[0];
            },
            start: 1.21,
            end: 1.6,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".orb3",
        { xPercent: -50, yPercent: -15 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line3", false)[0];
            },
            start: 1.14,
            end: 1.4,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".orb4",
        { xPercent: -50, yPercent: -25 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line4", false)[0];
            },
            start: 1.06,
            end: 1.31,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".orb4a",
        { xPercent: -50, yPercent: -25 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line4", false)[0];
            },
            start: 1.36,
            end: 1.5,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".orb4b",
        { xPercent: -50, yPercent: -25 },
        {
          motionPath: {
            path: function () {
              return MotionPathPlugin.convertToPath(".c1_line4", false)[0];
            },
            start: 1.41,
            end: 1.6,
          },
          ease: "none",
          yoyo: true,
          repeat: -1,
        },
        "orbs"
      )

      .fromTo(
        ".m1Orb",
        { scale: 0, transformOrigin: "50% 50%" },
        {
          duration: 0.8,
          scale: 1,
          ease: "back.out(3)",
          stagger: 0.15,
          overwrite: "auto",
        },
        "orbs"
      );

    // 鼠标悬停
    $(".process .item_block", el).on("mouseover", function () {
      $(this).addClass("active").siblings().removeClass("active");
    });

    // 烟花效果
    const burst = new mojs.Burst({
      left: 0,
      top: 0,
      radius: { 0: 200 },
      count: 20,
      degree: 360,
      children: {
        fill: { white: "#34E1FF" },
        duration: 2000,
      },
    });
    $(".process", el).on("click", function (e) {
      bursty(e.pageX, e.pageY);
    });
    setInterval(() => randomizedConfetti(), 1000);

    function bursty(x, y) {
      burst.tune({ x, y }).replay();
    }
    function randomizedConfetti() {
      let randomX = Math.floor(
        Math.random() * (document.body.clientWidth - 100)
      );
      let randomY = Math.floor(
        $(".process", el).offset().top + Math.random() * 400
      );
      bursty(randomX, randomY);
    }
  });
}

// wowinit
function initWow() {
  function afterReveal(el) {
    $(el).on("animationend", function (e) {
      $(el).addClass("complete");
    });

    $(".wowcb", el).on("animationend", function (e) {
      e.stopPropagation();
    });
  }
  window.wow = new WOW({
    callback: function (target) {
      $(".wowcb", target)[0] && afterReveal(target);
    },
  });
  window.wow.init();
}

// 搜索页
function pageSearch() {
  $(".bodysearch").each(function (i, el) {
    searchBox($(el));
  });
}

// 新闻页
function pageNews() {
  $(".bodynews, .bodynewspost").each(function (i, el) {
    pin(
      {
        containerSelector: ".pageContent",
        padding: {
          bottom: 60,
        },
      },
      $(".pageTarget .container_category", el)
    );
  });
}

// 搜索相关
function searchBox(el) {
  var $input = $(".search_box .searchform_input", el);
  var $close = $(".search_box .search_clear", el);
  var $result = $(".search_box .search_results", el);
  $(" .search_box", el).focusin(function () {
    $result.show();
  });
  blankClose($("#search_box"), function () {
    $result.hide();
  });
  $input.on("keyup", function () {
    if ($input.val() === "") {
      $close.hide();
    } else {
      $close.show();
    }
  });
  $close.click(function () {
    $(this).hide();
    $result.hide();
    $input.val("");
  });

  $input.on({
    keydown: function (ev) {
      var keyEn = (ev.key && ev.key === "Enter") || ev.keyCode === 13;
      if (keyEn) {
        var searchAddr = window.location.origin;
        var inpVal = $(this).val();
        window.location.href = `${searchAddr}/search/${inpVal}`;
      }
    },
    blur: function () {
      var _this = this;
      setTimeout(function () {
        $(_this).val("");
      }, 200);
    },
  });
}

// notice弹窗
function noticePopup() {
  $(".notice-popup").each(function (i, el) {
    var $slider = $(".swiper-container", el);
    var $moreBtn = $(".more", el);

    initSlider($slider, {
      direction: "vertical",
      loop: true,
      slidesPerView: 1,
      allowTouchMove: false,
      preventClicks: false,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      on: {
        slideChangeTransitionEnd: function () {
          var _href = $(".swiper-slide", $slider)
            .eq(this.realIndex)
            .attr("href");
          $moreBtn.attr("href", _href);
        },
      },
    });

    $slider.on("mouseover", function () {
      $(this)[0].swiper.autoplay.stop();
    });
    $slider.on("mouseout", function () {
      $(this)[0].swiper.autoplay.start();
    });

    // 底部隐藏
    var scOffsetH = 0;
    if ($(document).scrollTop() > scOffsetH) {
      gsap.set($(el), { y: 150, opacity: 0 });
    }
    $(document).on("scroll", function () {
      scOffsetH =
        $(document).outerHeight() -
        window.innerHeight -
        $("#footer").outerHeight() -
        100;
      var _y = $(document).scrollTop() > scOffsetH ? 150 : 0;
      var _opacity = $(document).scrollTop() > scOffsetH ? 0 : 1;

      gsap.to($(el), {
        y: _y,
        opacity: _opacity,
        ease: "Power1.easeOut",
        duration: 0.5,
      });
    });
  });
}

// qrcode
function initQrcode() {
  if (!$(".qrcode_wrapper")[0]) {
    return false;
  }
  $(".qrcode_wrapper").each(function (i, el) {
    var url = $(el).data("src");
    const qr = new QRCoder({
      data: url,
      size: 400,
    });
    var dataURL = qr.getDataURL();
    $(".qrcode", el).attr("src", dataURL);
  });
}

// 幻灯初始化
function initSlider($slider, options) {
  if (!$slider[0]) return;
  var settings = {
    roundLengths: true,
    watchOverflow: true,
    autoplay: false,
    loop: false,
    speed: 1000,
    direction: "horizontal",
    init: false,
  };

  var slickOpt = $.extend(settings, options);
  var myswiper = new Swiper($slider, slickOpt);

  myswiper.init();
  return myswiper;
}

// 动画结束后回调
function AnimationEnd(element, complete) {
  var t;
  var el = document.createElement("fakeelement");

  var animations = {
    animation: "animationend",
    OAnimation: "oAnimationEnd",
    MozAnimation: "animationend",
    WebkitAnimation: "webkitAnimationEnd",
  };

  for (t in animations) {
    if (el.style[t] !== undefined) {
      element.one(animations[t], function () {
        element.off(animations[t]);
        if (complete) complete.call($(element)[0]);
      });
    }
  }
}

// 点击空白关闭
function blankClose($elm, cb) {
  $(document).click(function (e) {
    var n = $elm;
    if (!n.is(e.target) && n.has(e.target).length === 0) {
      cb();
    }
  });
}

// 自定义滚动条
function initCustomScrollbar() {
  // scrollbar
  var $mscroll = $(".mCustomScrollbar");
  $mscroll.mCustomScrollbar("destroy");
  $mscroll.mCustomScrollbar();
}

// 自定义鼠标
class CustomCursor {
  constructor() {
    this.cursor = $(".view-indicator");
    this.cursorInner = $(".color-circle", this.cursor);
    this.hoverItems = $(".view-indicator-show");
    this.miniItems = $(".view-indicator-mini");
    this.hideItems = $(".view-indicator-hide");
    this.clientX = 0;
    this.clientY = 0;
    this.isMini = false;
  }

  init() {
    if (this.cursor[0]) {
      this.initCursor();
      this.initHovers();
      this.initMini();
      this.initHide();
    }
  }

  initCursor() {
    this.clientX = -100;
    this.clientY = -100;

    $(document).on("mousemove", (e) => {
      this.clientX = e.clientX;
      this.clientY = e.clientY;
    });
    const render = () => {
      gsap.to(this.cursor, {
        x: this.clientX,
        y: this.clientY,
        ease: "Power1.easeOut",
        duration: 0.5,
      });

      requestAnimationFrame(render);
    };
    render();
  }

  initHovers() {
    const handleMouseEnter = (e) => {
      if ($(e.target).closest(".view-indicator-mini")[0]) {
        this.isMini = true;
      } else {
        this.isMini = false;
      }
      this.cursor.removeClass("mini");
      gsap.to(this.cursorInner, {
        scale: 1,
        opacity: 1,
        ease: "Power1.easeOut",
        duration: 0.5,
      });
      e.stopPropagation();
    };
    const handleMouseLeave = (e) => {
      if (this.isMini) {
        gsap.to(this.cursorInner, {
          scale: 0.5,
          opacity: 0.5,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      } else {
        gsap.to(this.cursorInner, {
          scale: 0,
          opacity: 0,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      }
      this.cursor.addClass("mini");
    };
    this.hoverItems.each(function (i, item) {
      $(item).on("mouseenter", handleMouseEnter);
      $(item).on("mouseleave", handleMouseLeave);
    });
  }
  initMini() {
    this.cursor.addClass("mini");
    const handleMouseEnter = () => {
      gsap.to(this.cursorInner, {
        scale: 0.5,
        opacity: 0.5,
        ease: "Power1.easeOut",
        duration: 0.5,
      });
    };
    const handleMouseLeave = () => {
      gsap.to(this.cursorInner, {
        scale: 0,
        opacity: 0,
        ease: "Power1.easeOut",
        duration: 0.5,
      });
    };
    this.miniItems.each(function (i, item) {
      $(item).on("mouseenter", handleMouseEnter);
      $(item).on("mouseleave", handleMouseLeave);
    });
  }
  initHide() {
    const handleMouseLeave = (e) => {
      if ($(e.target).closest(".view-indicator-mini")[0]) {
        this.isMini = true;
      } else {
        this.isMini = false;
      }
      gsap.to(this.cursorInner, {
        scale: 1,
        opacity: 1,
        ease: "Power1.easeOut",
        duration: 0.5,
      });
    };
    const handleMouseEnter = () => {
      if (this.isMini) {
        gsap.to(this.cursorInner, {
          scale: 0.5,
          opacity: 0.5,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      } else {
        gsap.to(this.cursorInner, {
          scale: 0,
          opacity: 0,
          ease: "Power1.easeOut",
          duration: 0.5,
        });
      }
    };
    this.hideItems.each(function (i, item) {
      $(item).on("mouseenter", handleMouseEnter);
      $(item).on("mouseleave", handleMouseLeave);
    });
  }
}
// jq.pin
function pin(options, $dom) {
  var scrollY = 0;
  var elements = [];
  var disabled = false;
  var $window = $(window);

  options = $.extend(
    {
      distance: {
        bottom: 0,
      },
    },
    options
  );

  var recalculateLimits = function () {
    for (var i = 0, len = elements.length; i < len; i++) {
      var $this = elements[i];

      if (options.minWidth && $window.width() <= options.minWidth) {
        if ($this.parent().is(".pin-wrapper")) {
          $this.unwrap();
        }
        $this.css({
          width: "",
          left: "",
          top: "",
          position: "",
        });
        if (options.activeClass) {
          $this.removeClass(options.activeClass);
        }
        disabled = true;
        continue;
      } else {
        disabled = false;
      }

      var $container = options.containerSelector
        ? $this.closest(options.containerSelector)
        : $(document.body);
      var offset = $this.offset();
      var containerOffset = $container.offset();
      var parentOffset = $this.offsetParent().offset();

      if (!$this.parent().is(".pin-wrapper")) {
        $this.wrap('<div class="pin-wrapper"></div>');
      }

      var pad = $.extend(
        {
          top: 0,
          bottom: 0,
        },
        options.padding || {}
      );

      $this.data("pin", {
        pad: pad,
        from:
          (options.containerSelector ? containerOffset.top : offset.top) -
          pad.top,
        to:
          containerOffset.top +
          $container.height() -
          $this.outerHeight() -
          pad.bottom,
        end: containerOffset.top + $container.height(),
        parentTop: parentOffset.top,
      });

      $this.css({ width: $this.outerWidth() });
      $this.parent().css("height", $this.outerHeight());
    }
  };

  var onScroll = function () {
    if (disabled) {
      return;
    }

    scrollY = $window.scrollTop();

    var elmts = [];
    for (var i = 0, len = elements.length; i < len; i++) {
      var $this = $(elements[i]);
      var data = $this.data("pin");

      if (!data) {
        // Removed element
        continue;
      }

      elmts.push($this);

      var from = data.from;
      var to = data.to - options.distance.bottom;

      if (from + $this.outerHeight() > data.end) {
        $this.css("position", "");
        continue;
      }

      if (from < scrollY && to > scrollY) {
        !($this.css("position") === "fixed") &&
          $this
            .css({
              left: "",
              top: data.pad.top,
            })
            .css("position", "fixed");
        if (options.activeClass) {
          $this.addClass(options.activeClass);
        }
      } else if (scrollY >= to) {
        $this
          .css({
            left: "",
            top: to - data.parentTop,
          })
          .css("position", "absolute");
        if (options.activeClass) {
          $this.addClass(options.activeClass);
        }
      } else {
        $this.css({ position: "", top: "", left: "" });
        if (options.activeClass) {
          $this.removeClass(options.activeClass);
        }
      }
    }
    elements = elmts;
  };

  var update = function () {
    recalculateLimits();
    onScroll();
  };

  $dom.each(function (index, item) {
    var $item = $(item);
    var data = $item.data("pin") || {};

    if (data && data.update) {
      return;
    }
    elements.push($item);
    $("img", $item).one("load", recalculateLimits);
    data.update = update;
    $item.data("pin", data);
  });

  $window.scroll(onScroll);
  $window.resize(function () {
    $dom.removeAttr("style");
    recalculateLimits();
  });
  recalculateLimits();

  $window.on("load", update);

  return update;
}

// 个位补零
function zero(num, n) {
  if ((num + "").length >= n) return num;
  return zero("0" + num, n);
}

// 判断url
function checkURL(URL) {
  var str = URL;
  var Expression = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- ./?%&=]*)?/;
  var objExp = new RegExp(Expression);
  if (objExp.test(str) === true) {
    return true;
  } else {
    return false;
  }
}
