{{include './partial/header.tpl' data={title:'uemo产品价格',active:'price',bodyClass:'bodylist bodyprice'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage">
    <div class="pricelist">
      <div class="module_container wow">
        <div class="container_header">
          <div class="title">业务价格</div>
          <div class="subtitle">
            <i class="red">*</i> 去除底部UEmo版权：1000元 / 首年
          </div>
          <ul class="pro_type clearfix">
            <li class="item_type">
              <p class="title">基础版</p>
              <p class="price">
                <span class="num text">1999</span>
                <span class="unit text">元/首年</span>
              </p>
              <p class="renew">续费999元起</p>
            </li>
            <li class="item_type">
              <p class="title">增强版</p>
              <p class="price">
                <span class="num text">2999</span>
                <span class="unit text">元/首年</span>
              </p>
              <p class="renew">续费1499元起</p>
            </li>
            <li class="item_type">
              <p class="title">尊享版</p>
              <p class="price">
                <span class="num text">6999</span>
                <span class="unit text">元/首年</span>
              </p>
              <p class="renew">续费1499元起</p>
            </li>
          </ul>
        </div>
        <div class="rights">
          <div class="rightlist">
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">无限克隆列表/商品模块</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      通常在网站首页，您会希望尽量多的展示您的产品，或者仅挑选重要商品进行展示。在mo005模板中，您可以无限制的将列表，也就是商品克隆（复制）出来。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">PC+手机+微信导航</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      如果您有微信公众账号，需要通过公众号的菜单栏打开您的网站，实现“微站”的功能
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">8G阿里云空间服务器</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      8G阿里云空间服务器
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">绑定阿里云备案独立域名（香港免备案）</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      阿里云的线路有三个：杭州、北京、香港。您可以根据您的客户所在地、公司所在地选择相应的线路（一般会选择公司所在地就近的线路）。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">无限创建页面</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      模板本身的页面数量不能够满足您的需求时，您可以无限创建页面，添加您的业务/商品/优势等内容，能够将最全面的企业情况，展现给您的客户。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">月流量不限</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      月流量不限
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">在线客服专员跟踪指导</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      专业的客服专员，为您提供专业可靠的指导服务,沟通方式为：QQ：2852263145、电话：010-69557550。客服专员将为您提供视频、通话、文字、截图等多种方法的指导，您的任何问题，都可以迎刃而解，帮助您的网站快速上线。如果想快速联系到客服专员，为您解答疑问，您可以将客服专员加为好友（QQ：2852263145），方便联系。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">首页模块自由拖拽排序</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      UEMO的模板首页展示顺序如果不满足您的需求，您可以将模块自己拖拽，排列顺序，填充相应的内容，即可完全为成为您期望的首页样式。同时还能够保证您的网站是完全独一无二的，虽然是模板，但是有二次创作的可能。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">分享微信/微博</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      通过UEMO模板制作出的网站，支持分享到微信/微博等平台。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">首页留言版（搜集表单）</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      首页最下栏，有独立的留言板模块，客户可以通过填写信息，将客户的诉求“留言”给您。留言成功后，您的邮箱（邮箱为：您在后台，联系一栏中填写的邮箱账号）将会收到客户发送的“留言”。为您开发新业务，搜集客户需求，用户建议等提供方便的途径。同时简明扼要的信息填框，还能够搜集用户的重要信息，为分析用户，积累数据提供帮助。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">自定义导航</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      自定义导航
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">首页计数器模块</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      首页计数器模块
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">自定义视频板块付费开通</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      自定义视频板块付费开通
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="1"><i class="iconfont icon-zhengquetishi green"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">上传30篇图文资料</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      图文资料为新闻、项目、产品、业务等页面里填充的内容，也可理解为详情页。您需要提供图片和对应的文字，我们的服务专员会根据您提供的图片和文字，以多年的排版经验，将文字和图片上传到页面里，为您省去大量的时间（此项业务，不包含修改图片，和搜集图片，如不提供图片，将以您提供的文字为排版对象）。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="1"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">首页背景视频插件</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      视频能够快速的想用户展现您想表达的内容，并可以给用户留下深刻的印象。
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="1"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
            <div class="item_block clearfix">
              <div class="item_name fl">
                <span class="text">PC站VI调色</span>
                <i class="iconfont icon-wenhao tip">
                  <div class="item_toast">
                    <div class="desc">
                      PC站VI调色
                    </div>
                    <a class="link_box" href="#">
                      <span class="text">查看帮助</span>
                      <i class="iconfont icon-youjiantou"></i>
                    </a>
                  </div>
                </i>
              </div>
              <ul class="item_option fr">
                <li data-opt="0"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="1"><i class="iconfont icon-guanbicuowu red"></i></li>
                <li data-opt="2"><i class="iconfont icon-zhengquetishi green"></i></li>
              </ul>
            </div>
          </div>
          <div class="column clearfix">
            <div class="item_col"></div>
            <div class="item_col"></div>
            <div class="item_col"></div>
          </div>
          <div class="more">
            <span class="text">查看更多</span>
            <i class="iconfont icon-zhankai"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="service">
      <div class="module_container">
        <div class="container_header">
          <div class="title">附加服务</div>
        </div>
        <div class="container_content">
          <div class="content_list clearfix">
            <div class="item_block wow">
              <p class="title">专业人员上传资料</p>
              <p class="desc">需提供完整正确图文资料，编辑人员根据
                资料进行排版上传</p>
              <div class="getPrice">
                <span class="num">200</span>
                <span class="unit">元/篇</span>
              </div>
              <div class="iconfont"></div>
            </div>
            <div class="item_block wow" data-wow-delay="0.1s">
              <p class="title">首页Banner设计</p>
              <p class="desc">需提供图片素材及Slogan</p>
              <div class="getPrice">
                <span class="num">1000</span>
                <span class="unit">元/个</span>
              </div>
              <div class="iconfont"></div>
            </div>
            <div class="item_block wow" data-wow-delay="0.2s">
              <p class="title">内页Banner设计</p>
              <p class="desc">需提供图片素材及Slogan</p>
              <div class="getPrice">
                <span class="num">500</span>
                <span class="unit">元/个</span>
              </div>
              <div class="iconfont"></div>
            </div>
            <div class="item_block wow" data-wow-delay="0.3s">
              <p class="title">主题风格调试</p>
              <p class="desc">在现有模板框架下改变布局及颜色等</p>
              <div class="getPrice">
                <span class="num">4000</span>
                <span class="unit">元/套</span>
              </div>
              <div class="iconfont"></div>
            </div>
            <div class="item_block wow">
              <p class="title">整站VI调色</p>
              <p class="desc">根据您的VI进行调色</p>
              <div class="getPrice">
                <span class="num">500</span>
                <span class="unit">元/套</span>
              </div>
              <div class="iconfont"></div>
            </div>
            <div class="item_block wow" data-wow-delay="0.1s">
              <p class="title">首页背景图片换视频</p>
              <p class="desc">单个视频不超过10M，数量不超过5个，
                格式mp4</p>
              <div class="getPrice">
                <span class="num">500</span>
                <span class="unit">元/套</span>
              </div>
              <div class="iconfont"></div>
            </div>
            <div class="item_block wow" data-wow-delay="0.2s">
              <p class="title">SSL/TSL 证书安装服务</p>
              <p class="desc">代用户配置绑定证书（证书需用户自行申请
                并提供）作用：HTTPS化，使网站可信，防
                劫持、防篡改、防监听。</p>
              <div class="getPrice">
                <span class="num">1000</span>
                <span class="unit">元/次</span>
              </div>
              <div class="iconfont"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="payment">
      <div class="module_container">
        <div class="container_header">
          <div class="title">支付方式</div>
        </div>
        <div class="container_content">
          <div class="content_list clearfix">
            <div class="item_block bank wow">
              <div class="cir">
                <div class="c1 inner-cir"></div>
                <div class="c2 inner-cir"></div>
                <div class="c3 inner-cir"></div>
                <div class="c4 inner-cir"></div>
              </div>
              <div class="item_wrapper">
                <div class="title">银行转账</div>
                <div class="subtitle">建设银行-对公账户</div>
                <div class="bank_info">
                  <p>开户行：中国建设银行北京通州潞河支行</p>
                  <p>单位名称：北京优艺客文化传播有限公司</p>
                  <p>帐号：1100 1018 2000 5900 0967</p>
                </div>
              </div>
            </div>
            <div class="item_block alipay wow" data-wow-delay="0.1s">
              <div class="cir">
                <div class="c1 inner-cir"></div>
                <div class="c2 inner-cir"></div>
                <div class="c3 inner-cir"></div>
                <div class="c4 inner-cir"></div>
              </div>
              <div class="item_wrapper">
                <div class="title">支付宝转账-扫码付款</div>
                <div class="bank_info">
                  <p>支付宝账号：touch@uelike.com</p>
                  <p>支付宝名称：北京优艺客文化传播有限公司</p>
                </div>
                <div class="paycode">
                  <img src="./assets/images/alipay.jpg" alt="">
                </div>
              </div>
            </div>
            <div class="item_block wxpay wow" data-wow-delay="0.2s">
              <div class="cir">
                <div class="c1 inner-cir"></div>
                <div class="c2 inner-cir"></div>
                <div class="c3 inner-cir"></div>
                <div class="c4 inner-cir"></div>
              </div>
              <div class="item_wrapper">
                <div class="title">微信买单-扫描门店付款码</div>
                <div class="bank_info">
                  <p>门店名称：uemo</p>
                  <p>公司名称：北京优艺客文化传播有限公司</p>
                </div>
                <div class="paycode">
                  <img src="./assets/images/wxpay.jpg" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}