{{include './partial/header.tpl' data={title:'uemo模板列表详情',active:'template',bodyClass:'bodylistpost bodyunitpost'} }}
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pagePost">
    <div id="page_content" class="page_content">
      <div class="preview">
        <div class="control">
          <div class="control-part">
            <a class="arrow" href="./unit.html">
              <span class="arrow_anime"></span>
              <span class="text">返回列表</span>
            </a>
            <div class="page-switch">
              <div class="swiper-button-wrapper">
                <a class="swiper-button  swiper-button-prev iconfont icon-jiantou5-copy-copy" href="./unit_post.html" target="_self"></a>
                <a class="swiper-button  swiper-button-next iconfont icon-jiantou5-copy-copy-copy" href="./unit_post.html" target="_self"></a>
              </div>
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img src="./assets/images/1.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img src="./assets/images/1.jpg" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="control-part">
            <div class="device-select">
              <a href="javascript:;" class="desktop active" data-value="desktop">
                <i class="iconfont icon-desktop" aria-hidden="true"></i>
              </a>
              <a href="javascript:;" class="mobile" data-value="mobile">
                <i class="iconfont icon-phone" aria-hidden="true"></i>
              </a>
            </div>
            <div class="browse-btn iconfont"></div>
          </div>
        </div>
        <div id="contentArea" class="desktop">
          <div class="area_wrapper">
            <iframe id="contentFrame" class="contentFrame" data-src="http://page.uemo.net/index.php?id=5014#/"  src="http://page.uemo.net/index.php?id=5014#/" frameborder="no" border="0" marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>
            <div id="mask"></div>
          </div>
          <div class="mobile-code qrcode_wrapper" data-src="http://mo005-22017.mo5.line1.uemo.net">
            <img src="./assets/images/qrcode.png" width="175" height="175" alt="" class="qrcode" />
            <span class="text">扫描二维码预览手机站</span>
          </div>
        </div>
      </div>
      <div class="introduction mCustomScrollbar">
        <div class="intr-info">
          <div class="intr-head">
            <p class="title">内容-浅色001</p>
            <p class="subtitle">可在UEmo模板中进行编辑或使用</p>
          </div>
          <div class="intr-des">
            <p>页面可插入到UEmo模板的任何一个详情页当中，也可去掉导航及底部内容，完全独立展示。</p>
          </div>
          <a href="javascript:;" class="link_box">查看教程</a>
        </div>
        <div class="intr-rel">
          <h4>相关页面</h4>
          <div class="content_list clearfix">
            <div class="item_block">
              <a href="javascript:;">
                <div class="item_img">
                  <img src="./assets/images/case_bg1.jpg" alt="">
                </div>
              </a>
            </div>
            <div class="item_block">
              <a href="javascript:;">
                <div class="item_img">
                  <img src="./assets/images/case_bg1.jpg" alt="">
                </div>
              </a>
            </div>
            <div class="item_block">
              <a href="javascript:;">
                <div class="item_img">
                  <img src="./assets/images/case_bg1.jpg" alt="">
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>