{{include './partial/header.tpl' data={title:'uemo定制案例详情页',active:'case',bodyClass:'bodylistpost bodycustomcasepost'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent view-indicator-mini">
  <div class="npagePage postPage">
    <div class="postPage_header wow">
      <div class="back_btn">
        <a class="arrow" href="./custom_case.html">
          <span class="arrow_anime"></span>
          <span class="text">返回列表</span>
        </a>
      </div>
      <div class="introduction clearfix">
        <div class="intr_left fl">
          <div class="title ellipsis">ksher</div>
          <div class="subtitle ellipsis">跨境支付</div>
        </div>
        <div class="intr_right fr">
          <div class="info clearfix">
            <div class="item_info date fl">
              <p class="label">Date</p>
              <p class="value">2019/02</p>
            </div>
            <div class="item_info client fl">
              <p class="label">Client</p>
              <p class="value">Ksher泰国跨境支付</p>
            </div>
            <div class="link_btn view-indicator-show">
              <a class="link_box" href="https://www.uemo.net/">
                <span class="text">Link</span>
                <i class="iconfont icon-youjiantou"></i>
              </a>
            </div>
          </div>
          <div class="more">
            <div class="label">
              <span class="text">项目信息</span>
              <i class="iconfont icon-jiantou5-copy-copy-copy-copy"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="postPage_content">
      <img src="./assets/images/case_bg.jpg" alt="">
    </div>
    <div class="postPage_footer view-indicator-show">
      <div class="module_container">
        <a class="container" href="#">
          <div class="next_bg">
            <div class="bg" style="background-image:url(<%= require("./assets/images/custom_case.jpg") %>);"></div>
            <div class="mask"></div>
          </div>
          <div class="next_btn">
            <div class="label">下一个</div>
            <div class="next_title">天津大学建筑设计研究院</div>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="view-indicator">
  <div class="color-circle">
    <i class="iconfont icon-youjiantou"></i>
  </div>
</div>
{{include './partial/footer.tpl'}}