{{include './partial/header.tpl' data={title:'uemo组件列表',active:'unit',bodyClass:'bodylist bodyunit'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pageList">
    <div id="page_target" class="pageTarget">
      <div class="container_category">
        <div class="category_btn">
          <a href="javascript:;" class="item_btn active">浅色</a>
          <a href="javascript:;" class="item_btn">深色</a>
          <span class="move"></span>
        </div>
        <div class="category_wrapper">
        <a href="#" class=" active">
                    <span class="text">全部模板</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">艺术设计</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">摄影摄像</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">建筑园林</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">企业集团</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">数码家居</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">音体传播</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">休闲旅游</span>
                    <span class="num">121</span>
                  </a>
                  <a href="#" class="">
                    <span class="text">医疗教育</span>
                    <span class="num">121</span>
                  </a>
                  <div class="movedot"></div>
        </div>
      </div>
    </div>
    <div id="page_content" class="page_content">
      <div class="mlist template">
        <div class="module_container">
          <div class="container_header wow">
            <p class="title">音体传播</p>
            <p class="subtitle">
              收听完整的录音室专辑、视频游戏音轨或影片的声音设计
            </p>
          </div>
          <div class="container_content">
            <div class="content_wrapper">
              
              <div class="content_list clearfix">
                <div class="item_block  wow" data-wow-delay="0s">
                  <div class="item_img">
                    <a href="./unit_post.html" class="item_box">
                      <img src="./assets/images/1.jpg" alt="" />
                    </a>
                  </div>
                  <div class="item_wrapper">
                    <div class="item_info">
                      <p class="title ellipsis">视频制作网站</p>
                      <p class="subtitle">
                        <span class="num">编号</span>
                        <span class="id">mo005_12446</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {{include './partial/pages.tpl'}}
        </div>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}