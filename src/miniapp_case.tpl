{{include './partial/header.tpl' data={title:'名片案例',active:'case',bodyClass:'bodylist bodyminiapp'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <div class="npagePage pageList">
    <div id="page_content" class="page_content">
      <div class="mlist miniapp">
        <div class="module_container">
        <div class="container_header">
          <p class="title">好看智能名片</p>
          <p class="subtitle">品牌营销拓客工具</p>
          <div class="t-btn t-btn--primary js-btn-circle is-appear more">
            <a href="http://www.uemox.com/" target="_blank"><span class="t-btn__panel__before"></span> <span class="t-btn__label"><span class="t-btn__label__masked">了解更多</span></span> <span class="t-btn__circle__wrapper"><span class="t-btn__circle js-btn-circle-el"></span></span> <span class="t-btn__panel"></span> <span class="t-btn__label__circle"><span class="t-btn__label__circle__masked">了解更多</span></span> <span class="t-btn__panel__after"></span></a>
          </div>
        </div>
          <div class="container_content">
            <div class="content_list clearfix">
              <% for(var i = 0; i < 5; i++){ %>
                <div class="item_block">
                  <div class="item_box">
                    <div class="item_wrapper">
                      <div class="head">
                        <div class="logo">
                          <img src="./assets/images/logo.png" alt="">
                        </div>
                        <div class="name ellipsis">优艺客</div>
                      </div>
                      <div class="info">
                        <p class="title ellipsis">韩雪冬</p>
                        <p class="subtitle ellipsis">ceo/创始人</p>
                      </div>
                      <div class="des">
                        优艺客拥有着研发精神和创新精神的团队组成。以前瞻性的产品和极具前卫精神的专业团队服务于上百家
                      </div>
                    </div>
                    <div class="item_img">
                      <div class="image">
                        <img src="./assets/images/miniapp_image.jpg" alt="">  
                      </div>
                      <div class="qrcode">
                        <img src="./assets/images/miniapp_qr.jpg" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="try_btn t-btn t-btn--primary js-btn-circle is-appear t-btn2 open-popup" data-type="miniapp">
                      <span class="t-btn__panel__before"></span> <span class="t-btn__label"><span class="t-btn__label__masked">我也要用</span></span> <span class="t-btn__circle__wrapper"><span class="t-btn__circle js-btn-circle-el"></span></span> <span class="t-btn__panel"></span> <span class="t-btn__label__circle"><span class="t-btn__label__circle__masked">我也要用</span></span> <span class="t-btn__panel__after"></span>
                  </div>
                </div>
              <% } %>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="popup_qr pop-miniapp">
  <div class="fixed-container">
    <img src="./assets/images/miniapp-code.png" alt="">
    <p>微信扫码，马上使用</p>
  </div>
</div>
{{include './partial/footer.tpl'}}