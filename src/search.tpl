{{include './partial/header.tpl' data={title:'uemo搜索',active:'search',bodyClass:'bodylist bodysearch'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
    <div class="npagePage pageList">
        <div id="search_box" class="search_box wow">
            <div class="search_content">
                <div class="search_bar">
                    <div class="search_bar_wrapper">
                        <form action="" id="searchform">
                            <input
                                type="text"
                                placeholder="网站"
                                class="searchform_input"
                            />
                            <button
                                id="searchform_submit"
                                class="searchform_submit"
                                disabled
                            >
                                <div class="iconfont icon-sousuo-copy"></div>
                            </button>
                            <div class="search_clear">
                                <div class="search_clear_wrapper">
                                    <div class="line top">
                                        <span class="rect top"></span>
                                    </div>
                                    <div class="line bottom">
                                        <span class="rect bottom"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="search_results">
                    <ul class="drop-list">
                        <li>案例</li>
                        <li>案例实例</li>
                        <li>案例价格</li>
                    </ul>
                    <section class="result_block">
                        <h4>最近搜索</h4>
                        <div class="result_words">
                            <a href="#">网站备案</a>
                            <a href="#" class="active">模板</a>
                            <a href="#">价格</a>
                        </div>
                    </section>
                    <section class="result_block">
                        <h4>热门搜索</h4>
                        <div class="result_words">
                            <a href="#">如何解绑域名</a>
                            <a href="#">域名解析流程</a>
                            <a href="#">建站前的准备工作</a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div id="pageCategory" class="pageCategory wow">
            <a href="#" class="item_category active">
                <span class="text">分类</span>
                <span class="num">
                    (<span>15</span>)
                </span>
            </a>
            <a href="#" class="item_category">
                <span class="text">模板</span>
                <span class="num">
                    (<span>2</span>)
                </span>
            </a>
            <a href="#" class="item_category">
                <span class="text">新闻</span>
                <span class="num">
                    (<span>10</span>)
                </span>
            </a>
            <a href="#" class="item_category">
                <span class="text">案例</span>
                <span class="num">
                    (<span>1</span>)
                </span>
            </a>
        </div>
        <div id="history" class="history wow">
            <div class="module_container">
                <div class="history_bar">
                    <h3 class="title">最近搜索</h3>
                    <div class="history_btn">
                        <a href="javascript:;" class="clear">清空</a>
                    </div>
                </div>
                <div class="history_tags">
                    <div class="history_tags_wrapper">
                        <a href="#">网站备案</a>
                        <a href="#" class="active">模板</a>
                        <a href="#">价格</a>
                    </div>
                </div>
                <div class="history_multiple">
                    <h3 class="title">最近搜索</h3>
                    <ul class="problem_list">
                        <li class="item_problem ellipsis">
                            <a href="#">个人、企业网站建设常见问题 Q&A</a>
                        </li>
                        <li class="item_problem ellipsis">
                            <a href="#"
                                >企业建站 |
                                需要给建站公司提供的内容（超详细）</a
                            >
                        </li>
                        <li class="item_problem ellipsis">
                            <a href="#">网站模板 | 【域名】相关所有常见问题</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="pageResult" class="pageResult">
            <div class="module_container">
                <div class="container_content">
                    <div class="module template wow">
                        <h3 class="title">模板</h3>
                        <div class="content_wrapper">
                            <div class="content_list">
                                <div class="item_block">
                                    <a
                                        href="./template_post.html"
                                        class="item_box clearfix"
                                    >
                                        <div class="item_img fl">
                                            <img
                                                src="./assets/images/1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper fr">
                                            <div class="item_info">
                                                <div class="title">
                                                    <span>视频制作</span>
                                                    <span class="keyword"
                                                        >网站</span
                                                    >
                                                </div>
                                                <div class="id">
                                                    mo005_12446
                                                </div>
                                            </div>
                                            <div class="item_des">
                                                <p>
                                                    因为我们用的是国内相对稳定的阿里云服务器，那么您的网站也是通过阿里云服务器来运转的，您网站上的图片、文字、视频等资料也是上传到阿里云的服务器上的。所以只有通过阿里云的备案...
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item_block">
                                    <a
                                        href="./template_post.html"
                                        class="item_box clearfix"
                                    >
                                        <div class="item_img fl">
                                            <img
                                                src="./assets/images/1.jpg"
                                                alt=""
                                            />
                                        </div>
                                        <div class="item_wrapper fr">
                                            <div class="item_info">
                                                <div class="title">
                                                    <span>视频制作</span>
                                                    <span class="keyword"
                                                        >网站</span
                                                    >
                                                </div>
                                                <div class="id">
                                                    mo005_12446
                                                </div>
                                            </div>
                                            <div class="item_des">
                                                <p>
                                                    因为我们用的是国内相对稳定的阿里云服务器，那么您的网站也是通过阿里云服务器来运转的，您网站上的图片、文字、视频等资料也是上传到阿里云的服务器上的。所以只有通过阿里云的备案...
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="module news wow">
                        <h3 class="title">新闻</h3>
                        <div class="content_wrapper">
                            <div class="content_list">
                                <div class="item_block">
                                    <a
                                        href="./news_post.html"
                                        class="item_box clearfix"
                                    >
                                        <div class="item_wrapper">
                                            <div class="item_info">
                                                <div class="title">
                                                    <span class="keyword"
                                                        >网站</span
                                                    >
                                                    <span
                                                        >模板优化 |
                                                        5号网站模板后台功能详解</span
                                                    >
                                                </div>
                                                <div class="date_wrap">
                                                    <span class="year"
                                                        >2016</span
                                                    >
                                                    <i class="time-connect"
                                                        >-</i
                                                    >
                                                    <span class="m">12</span>
                                                    <i class="time-connect"
                                                        >-</i
                                                    >
                                                    <span class="d">14</span>
                                                </div>
                                            </div>
                                            <div class="item_des">
                                                <p>
                                                    因为我们用的是国内相对稳定的阿里云服务器，那么您的网站也是通过阿里云服务器来运转的，您网站上的图片、文字、视频等资料也是上传到阿里云的服务器上的。所以只有通过阿里云的备案...
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item_block">
                                    <a
                                        href="./news_post.html"
                                        class="item_box clearfix"
                                    >
                                        <div class="item_wrapper">
                                            <div class="item_info">
                                                <div class="title">
                                                    <span class="keyword"
                                                        >网站</span
                                                    >
                                                    <span
                                                        >模板优化 |
                                                        5号网站模板后台功能详解</span
                                                    >
                                                </div>
                                                <div class="date_wrap">
                                                    <span class="year"
                                                        >2016</span
                                                    >
                                                    <i class="time-connect"
                                                        >-</i
                                                    >
                                                    <span class="m">12</span>
                                                    <i class="time-connect"
                                                        >-</i
                                                    >
                                                    <span class="d">14</span>
                                                </div>
                                            </div>
                                            <div class="item_des">
                                                <p>
                                                    因为我们用的是国内相对稳定的阿里云服务器，那么您的网站也是通过阿里云服务器来运转的，您网站上的图片、文字、视频等资料也是上传到阿里云的服务器上的。所以只有通过阿里云的备案...
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{include './partial/pages.tpl'}}
                </div>
            </div>
        </div>
    </div>
</div>
{{include './partial/footer.tpl'}}
