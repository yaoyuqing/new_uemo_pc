{{include './partial/header.tpl' data={title:'uemo首页',bodyClass:'bodyindex'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
  <!-- slider -->
  <div id="mslider">
    <div class="container-content swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide item_block">
          <canvas class="renderSurface" data-src="./assets/images/LDR_LLL1_0.png"></canvas>
          <div class="item_wrapper">
            <p class="title"><span>UEmo</span>极速建站</p>
            <div class="subtitle">
              拥有一个属于您的高品质网站，从现在开始
            </div>
          </div>
          <div class="item_btn">
            <div class="t-btn t-btn--primary js-btn-circle is-appear">
              <a href="#"><span class="t-btn__panel__before"></span> <span class="t-btn__label"><span class="t-btn__label__masked">免费试用</span></span> <span class="t-btn__circle__wrapper"><span class="t-btn__circle js-btn-circle-el"></span></span> <span class="t-btn__panel"></span> <span class="t-btn__label__circle"><span class="t-btn__label__circle__masked">免费试用</span></span> <span class="t-btn__panel__after"></span></a>
            </div>
            <div class="t-btn t-btn--primary js-btn-circle is-appear t-btn2">
              <a href="#"><span class="t-btn__panel__before"></span> <span class="t-btn__label"><span class="t-btn__label__masked">我们的客户</span></span> <span class="t-btn__circle__wrapper"><span class="t-btn__circle js-btn-circle-el"></span></span> <span class="t-btn__panel"></span> <span class="t-btn__label__circle"><span class="t-btn__label__circle__masked">我们的客户</span></span> <span class="t-btn__panel__after"></span></a>
            </div>
          </div>
        </div>
        <div class="swiper-slide item_block">
          <canvas class="renderSurface" data-src="./assets/images/LDR_LLL1_0.png"></canvas>
          <div class="item_wrapper">
            <p class="title">智能名片 + UEmo建站</p>
            <div class="subtitle">
              品牌运营拓客工具-可直接绑定UEmo建站产品
            </div>
          </div>
          <div class="item_btn">
            <div class="t-btn t-btn--primary js-btn-circle is-appear">
              <a href="https://www.uemox.com" target="_blank"><span class="t-btn__panel__before"></span> <span class="t-btn__label"><span class="t-btn__label__masked">什么是智能名片</span></span> <span class="t-btn__circle__wrapper"><span class="t-btn__circle js-btn-circle-el"></span></span> <span class="t-btn__panel"></span> <span class="t-btn__label__circle"><span class="t-btn__label__circle__masked">什么是智能名片</span></span> <span class="t-btn__panel__after"></span></a>
            </div>
            <div class="code_btn">
              <div class="t-btn t-btn--primary js-btn-circle is-appear t-btn2">
                <a href="#"><span class="t-btn__panel__before"></span> <span class="t-btn__label"><span class="t-btn__label__masked">扫描进入小程序</span></span> <span class="t-btn__circle__wrapper"><span class="t-btn__circle js-btn-circle-el"></span></span> <span class="t-btn__panel"></span> <span class="t-btn__label__circle"><span class="t-btn__label__circle__masked">扫描进入小程序</span></span> <span class="t-btn__panel__after"></span></a>
              </div>
              <div class="mini-code">
                <img src="./assets/images/miniapp-code.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-button-wrapper">
      <div class="swiper-button swiper-button-prev"></div>
      <div class="swiper-button swiper-button-next"></div>
    </div>
  </div>
  <!-- mcounter -->
  <div id="mcounter" class="mcounter mlist hide">
    <div class="module_container">
      <div class="content_wrapper">
        <ul class="content_list clearfix">
          <li class="item_block">
            <p class="number" data-counter-value="24682">
              <span class="unit cross"></span>
            </p>
            <p class="title">建站数量</p>
          </li>
          <li class="item_block">
            <p class="number" data-counter-value="114">
              <span class="unit">套</span>
            </p>
            <p class="title">建站产品</p>
          </li>
          <li class="item_block">
            <p class="number" data-counter-value="14">
              <span class="unit">年</span>
            </p>
            <p class="title">WEB服务</p>
          </li>
          <li class="item_block">
            <p class="number" data-counter-value="376">
              <span class="unit cross"></span>
            </p>
            <p class="title">覆盖城市</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- template -->
  <div id="template" class="template module">
    <div class="module_container">
      <div class="container_header wow">
        <p class="title">UEmo网站模板</p>
        <p class="subtitle">
          10年建站经验+800家企业客户+知名建站团队=UEmo高端企业建站
        </p>
      </div>
      <div class="container_category wow">
        <div class="header_btn">
          <a href="javascript:;" class="choice">
            <span>选择行业</span>
            <i class="iconfont icon-fenlei"></i>
          </a>
          <a href="javascript:;" class="random">
            <i class="iconfont icon-huanyihuan"></i>
            <span>随机欣赏</span>
          </a>
        </div>
        <div class="category_menu">
          <div class="category_wrapper">
            <div class="title">下面有您的行业吗？</div>
            <div class="category_content">
              <a href="//www.uemo.net/list/id/3243/">艺术设计</a>
              <a href="//www.uemo.net/list/id/3242/">摄影摄像</a>
              <a href="//www.uemo.net/list/id/3240/">建筑园林</a>
              <a href="//www.uemo.net/list/id/3239/">企业集团</a>
              <a href="//www.uemo.net/list/id/3241/">数码家居</a>
              <a href="//www.uemo.net/list/id/26794/">音体传播</a>
              <a href="//www.uemo.net/list/id/87267/">休闲旅游</a>
              <a href="//www.uemo.net/list/id/3238/">医疗教育</a>
              <a href="//www.uemo.net/list/id/3247/">服饰礼服</a>
              <a href="//www.uemo.net/list/id/3237/">餐饮茶酒</a>
              <a href="//www.uemo.net/list/id/34305/">农工林牧</a>
            </div>
          </div>
          <div class="add_category">
            <div class="title">怎么没有我的行业</div>
            <div class="form_group">
              <input type="text" class="text" placeholder="您的行业是？" />
            </div>
          </div>
        </div>
      </div>
      <div class="container_content" data-files="https://static.uemo.net">
        <div class="content_wrapper">
          {{include './partial/template_list.tpl'}}
          <a href="#" class="more wow">
            <span>查看全部</span>
            <i class="iconfont icon-jiantou5-copy-copy-copy"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- case -->
  <div id="case" class="case module">
    <div class="module_container">
          <div class="container_header wow">
            <p class="title">这些摄影摄像机构都在用</p>
            <p class="subtitle">
              超1000家行业佼佼者选择UEMO极速建站
            </p>
          </div>
          <div class="container_category wow">
            <div class="category_wrapper swiper-container">
              <div class="swiper-wrapper">
                <a href="#" data-cats="cream" class="swiper-slide">摄影摄像</a>
                <a href="#" class="swiper-slide">建筑装饰</a>
                <a href="#" class="swiper-slide">企业集团</a>
                <a href="#" class="swiper-slide">医疗教育</a>
                <a href="#" class="swiper-slide">智能科技</a>
                <a href="#" class="swiper-slide">品牌营销</a>
                <a href="#" class="swiper-slide">生活家居</a>
                <a href="#" class="swiper-slide">智能科技</a>
                <a href="#" class="swiper-slide">品牌营销</a>
                <a href="#" class="swiper-slide">生活家居</a>
              </div>
            </div>
            <div class="swiper-button swiper-button-prev"></div>
            <div class="swiper-button swiper-button-next"></div>
          </div>
          <div class="container_content wow">
            <div class="content_images">
              <div class="content_wrapper swiper-container">
                <div class="content_list swiper-wrapper">
                  <div class="item_block swiper-slide" data-wow-delay="0.1s">
                    <a href="./template_case_post.html" class="item_box">
                      <div class="item_img">
                        <img src="./assets/images/case_img.jpg" alt="" />
                      </div>
                    </a>
                    <div class="pro-mask">
                      <a href="./template_case_post.html" class="preview" target="_blank">浏览网站</a>
                      
                    </div>
                    <div class="qrcode_btn">
                      <div class="icon">
                        <i class="iconfont icon-erweima"></i>
                      </div>
                      <div class="qrcode_wrapper" data-src="https://www.uemo.net/">
                        <img src="./assets/images/qrcode.png" alt="" class="qrcode" />
                        <span class="text">请使用微信扫一扫查看</span>
                      </div>
                    </div>
                  </div>
                  <div class="item_block swiper-slide" data-wow-delay="0.2s">
                    <a href="./template_case_post.html" class="item_box">
                      <div class="item_img">
                        <img src="./assets/images/case_img.jpg" alt="" />
                      </div>
                    </a>
                    <div class="pro-mask">
                      <a href="./template_case_post.html" class="preview" target="_blank">浏览网站</a>
                      
                    </div>
                    <div class="qrcode_btn">
                      <div class="icon">
                        <i class="iconfont icon-erweima"></i>
                      </div>
                      <div class="qrcode_wrapper" data-src="https://www.uemo.net/">
                        <img src="./assets/images/qrcode.png" alt="" class="qrcode" />
                        <span class="text">请使用微信扫一扫查看</span>
                      </div>
                    </div>
                  </div>
                  <div class="item_block swiper-slide" data-wow-delay="0s">
                    <a href="./template_case_post.html" class="item_box">
                      <div class="item_img">
                        <img src="./assets/images/case_img.jpg" alt="" />
                      </div>
                    </a>
                    <div class="pro-mask">
                      <a href="./template_case_post.html" class="preview" target="_blank">浏览网站</a>
                      
                    </div>
                    <div class="qrcode_btn">
                      <div class="icon">
                        <i class="iconfont icon-erweima"></i>
                      </div>
                      <div class="qrcode_wrapper" data-src="https://www.uemo.net/">
                        <img src="./assets/images/qrcode.png" alt="" class="qrcode" />
                        <span class="text">请使用微信扫一扫查看</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="button-wrapper prev">
                  <div class="swiper-button-prev"></div>
                </div>
                <div class="button-wrapper next">
                  <div class="swiper-button-next"></div>
                </div>
                <div class="swiper-pagination"></div>
              </div>
            </div>
            <div class="content_info"> 
              <div class="content_wrapper swiper-container">
                <div class="content_list swiper-wrapper">
                  <div class="item_block swiper-slide">
                    <div class="item_info">
                      <p class="title">
                        <span class="text ellipsis">耐飞影视</span>
                      </p>
                      <p class="subtitle">
                        <span class="text ellipsis">北京网站建设案例，企业建站案例</span>
                      </p>
                    </div>
                    <div class="item_index">
                      <span class="current_num">1</span>
                    </div>
                    <div class="item_des">
                      <p>
                        耐飞是一家致力将好故事打造成为好作品的影视公司。耐飞的官网采用半定制的形式呈现，在设计上基于影视公司的特点，主要以“视频播放图标”的元素设计按钮，表现出影视公司的特点；动态箭头曲线的穿插，将所有版块生动的串联在一起，极具现代感和设计感。不同的内容采用不同的展现方法，和动效切换形式，给访客更大的新鲜感和探索欲望；以统一的原素和色调进行关联，让网站虽处处不同，又处处和谐统一。
                      </p>
                    </div>
                  </div>
                  <div class="item_block swiper-slide">
                    <div class="item_info">
                      <p class="title ellipsis">
                        <span class="text">耐飞影视</span>
                      </p>
                      <p class="subtitle">
                        <span class="text">北京网站建设案例，企业建站案例</span>
                      </p>
                    </div>
                    <div class="item_index">
                      <span class="current_num">2</span>
                    </div>
                    <div class="item_des">
                      <p>
                        耐飞是一家致力将好故事打造成为好作品的影视公司。耐飞的官网采用半定制的形式呈现，在设计上基于影视公司的特点，主要以“视频播放图标”的元素设计按钮，表现出影视公司的特点；动态箭头曲线的穿插，将所有版块生动的串联在一起，极具现代感和设计感。不同的内容采用不同的展现方法，和动效切换形式，给访客更大的新鲜感和探索欲望；以统一的原素和色调进行关联，让网站虽处处不同，又处处和谐统一。
                      </p>
                    </div>
                  </div>
                  <div class="item_block swiper-slide">
                    <div class="item_info">
                      <p class="title ellipsis">
                        <span class="text">耐飞影视</span>
                      </p>
                      <p class="subtitle">
                        <span class="text">北京网站建设案例，企业建站案例</span>
                      </p>
                    </div>
                    <div class="item_index">
                      <span class="current_num">3</span>
                    </div>
                    <div class="item_des">
                      <p>
                        耐飞是一家致力将好故事打造成为好作品的影视公司。耐飞的官网采用半定制的形式呈现，在设计上基于影视公司的特点，主要以“视频播放图标”的元素设计按钮，表现出影视公司的特点；动态箭头曲线的穿插，将所有版块生动的串联在一起，极具现代感和设计感。不同的内容采用不同的展现方法，和动效切换形式，给访客更大的新鲜感和探索欲望；以统一的原素和色调进行关联，让网站虽处处不同，又处处和谐统一。
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div>
  <!-- client -->
  <div id="client" class="client module">
    <div class="module_container">
      <div class="container_header wow">
        <p class="title">我们的客户都怎么说</p>
        <p class="subtitle">
          10年建站经验+800家企业客户+知名建站团队=UEmo 高端企业建站
        </p>
        <a class="more" href="./client.html">更多</a>
      </div>
      <div class="container_content">
        <div class="content_wrapper swiper-container">
          <div class="content_list swiper-wrapper">
            <div class="item_block swiper-slide clearfix wow" data-wow-delay="0.1s">
              <div class="item_img">
                <img src="./assets/images/team_img.jpg" alt="" />
              </div>
              <div class="item_wrapper">
                <p class="title">聂心远</p>
                <p class="subtitle">黑金经纪创始人兼董事长</p>
                <div class="des mCustomScrollbar">
                  创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                  创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                  创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                </div>
              </div>
              <div class="autograph">
                <img src="./assets/images/team_autograph_03.jpg" alt="" />
              </div>
            </div>
            <div class="item_block swiper-slide clearfix wow" data-wow-delay="0.2s">
              <div class="item_img">
                <img src="./assets/images/team_img.jpg" alt="" />
              </div>
              <div class="item_wrapper">
                <p class="title">聂心远</p>
                <p class="subtitle">黑金经纪创始人兼董事长</p>
                <div class="des mCustomScrollbar">
                  创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                </div>
              </div>
              <div class="autograph">
                <img src="./assets/images/team_autograph_03.jpg" alt="" />
              </div>
            </div>
            <div class="item_block swiper-slide clearfix wow" data-wow-delay="0s">
              <div class="item_img">
                <img src="./assets/images/team_img.jpg" alt="" />
              </div>
              <div class="item_wrapper">
                <p class="title">聂心远</p>
                <p class="subtitle">黑金经纪创始人兼董事长</p>
                <div class="des mCustomScrollbar">
                  创业就像人生一样，都是孤独的。不忘初心，坚定的走下去，这件事就可能会成功。创业四年，年轻的黑金经纪经历了由黑至金，暗夜向阳。UEMO的设计真正实现了“黑金”精神的视觉传达，可以让更多的年轻人在互联网世界看见“黑金”，感受“黑金”。
                </div>
              </div>
              <div class="autograph">
                <img src="./assets/images/team_autograph_03.jpg" alt="" />
              </div>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- news -->
  <div id="news" class="news module">
    <div class="module_container">
      <div class="container_header wow">
        <p class="title">帮助中心</p>
      </div>
      <div class="container_category wow">
        <div class="category-wrapper news-swiper-pagination">
          <a href="#">常见问题</a>
          <a href="#">网站优化</a>
          <a href="#">建站说明</a>
          <a href="#">实用工具</a>
          <a href="#">后台详解</a>
        </div>
        <a href="./news.html" class="more">更多</a>
      </div>
      <div class="container_content">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <div class="content_wrapper swiper-slide">
              <div class="content_list clearfix">
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      个人、企业网站建设常见问题 Q&A
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板优化 |
                      5号网站模板后台功能详解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板 | UEMO·功 能 详 解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板安装SSL安全证书的说明
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站 |
                      需要给建站公司提供的内容...
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站所需的实用工具
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="content_wrapper swiper-slide">
              <div class="content_list clearfix">
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      个人、企业网站建设常见问题 Q&A
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板优化 |
                      5号网站模板后台功能详解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板 | UEMO·功 能 详 解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板安装SSL安全证书的说明
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站 |
                      需要给建站公司提供的内容...
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站所需的实用工具
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="content_wrapper swiper-slide">
              <div class="content_list clearfix">
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      个人、企业网站建设常见问题 Q&A
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板优化 |
                      5号网站模板后台功能详解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板 | UEMO·功 能 详 解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板安装SSL安全证书的说明
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站 |
                      需要给建站公司提供的内容...
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站所需的实用工具
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="content_wrapper swiper-slide">
              <div class="content_list clearfix">
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      个人、企业网站建设常见问题 Q&A
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板优化 |
                      5号网站模板后台功能详解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板 | UEMO·功 能 详 解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板安装SSL安全证书的说明
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站 |
                      需要给建站公司提供的内容...
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站所需的实用工具
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="content_wrapper swiper-slide">
              <div class="content_list clearfix">
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      个人、企业网站建设常见问题 Q&A
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板优化 |
                      5号网站模板后台功能详解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板 | UEMO·功 能 详 解
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      网站模板安装SSL安全证书的说明
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.1s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站 |
                      需要给建站公司提供的内容...
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
                <div class="item_block wow" data-wow-delay="0.2s">
                  <a href="./news_post.html">
                    <p class="title ellipsis">
                      企业建站所需的实用工具
                    </p>
                    <div class="des">
                      魔艺极速建站系统，是一款易用，省事，省心的网站建设利器，您对的切疑问，都会在本篇里得到解答。
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- mcustomize -->
  <div id="mcustomize" class="mcustomize">
    <div class="module_container">
      <div class="container_content">
        <div class="header wow">
          <div class="title">开启您的建站之旅吧</div>
        </div>
        <div class="description wow" data-wow-delay="0.1s">
          我们深知创业艰难，守业更难，一个容易让意向客户一秒铭记品牌的官网，助您企业上升，一个视觉美观的官网，能够在竞价推广时在众多网站中脱颖而出，一个便于优化和更新的官网，能为您省下资金做更有用的事；一次试用，能让未来的您，感激现在自己的决定
        </div>
        <a href="/template/" class="free_btn wow" data-wow-delay="0.2s">免费试用</a>
      </div>
    </div>
  </div>
</div>
{{include './partial/footer.tpl'}}