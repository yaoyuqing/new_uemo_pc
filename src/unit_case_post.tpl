<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8" />
    <meta name="divport" content="width=device-width, initial-scale=1.0" />
    <title>组件展示</title>
  </head>

  <body class="unit-case-post">
    <section id="control" class="control">
      <div class="control-menu">
        <div class="device-select">
          <a href="javascript:;" class="desktop active" data-value="desktop">
            <i class="iconfont icon-desktop" aria-hidden="true"></i>
          </a>
          <a href="javascript:;" class="mobile" data-value="mobile">
            <i class="iconfont icon-phone" aria-hidden="true"></i>
          </a>
        </div>
      </div>
      <div id="logo" class="logo">
        <a href="./index.html">
          <i class="iconfont icon-uemologo-copy"></i>
        </a>
      </div>
      <div id="try_btn" class="try_btn">
        <a href="https://www.uemo.net/page/diy.html">试用</a>
      </div>
    </section>
    <div id="contentArea" class="desktop">
      <div class="area_wrapper">
        <iframe id="contentFrame" class="contentFrame" data-src="https://www.uemo.net/page/diy.html" src="https://www.uemo.net/page/diy.html" frameborder="no" border="0" marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>
        <div id="mask"></div>
      </div>
      <div class="mobile-code qrcode_wrapper" data-src="http://mo005-21507.mo5.line1.uemo.net">
        <img src="./assets/images/qrcode.png" width="175" height="175" alt="" class="qrcode" />
        <span class="text">扫描二维码预览手机站</span>
      </div>
    </div>
  </body>

</html>