{{include './partial/header.tpl' data={title:'uemo帮助中心',active:'news',bodyClass:'bodynews'} }}
<!-- 内容 -->
<div id="sitecontent" class="sitecontent">
    <div id="banner" class="banner">
        <div class="module_container">
            <div class="container_header wow">
                <p class="title">帮助中心</p>
            </div>
            <div class="container_search search_box wow">
                <div class="search_content">
                    <div class="search_bar">
                        <div class="search_bar_wrapper">
                            <form action="" id="searchform">
                                <input
                                    type="text"
                                    placeholder="搜索"
                                    class="searchform_input"
                                />
                                <button
                                    id="searchform_submit"
                                    class="searchform_submit"
                                    disabled
                                >
                                    <div
                                        class="iconfont icon-sousuo-copy"
                                    ></div>
                                </button>
                                <div class="search_clear">
                                    <div class="search_clear_wrapper">
                                        <div class="line top">
                                            <span class="rect top"></span>
                                        </div>
                                        <div class="line bottom">
                                            <span class="rect bottom"></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="search_results">
                        <ul class="drop-list">
                            <li>案例</li>
                            <li>案例实例</li>
                            <li>案例价格</li>
                        </ul>
                        <section class="result_block">
                            <h4>最近搜索</h4>
                            <div class="result_words">
                                <a href="#">网站备案</a>
                                <a href="#" class="active">模板</a>
                                <a href="#">价格</a>
                            </div>
                        </section>
                        <section class="result_block">
                            <h4>热门搜索</h4>
                            <div class="result_words">
                                <a href="#">如何解绑域名</a>
                                <a href="#">域名解析流程</a>
                                <a href="#">建站前的准备工作</a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="container_tags wow">
                <a href="#">域名解析</a>
                <a href="#">网站添加视频</a>
                <a href="#">网站如何优化</a>
                <a href="#">如何选择模板</a>
                <a href="#">网站使用流程</a>
            </div>
        </div>
    </div>
    <div id="pageContent" class="pageContent">
        <div id="pageTarget" class="pageTarget wow">
            <div class="container_category">
                <div class="category_wrapper">
                    <a href="#" class="active">全部</a>
                    <a href="#">选择模板</a>
                    <a href="#">SEO</a>
                    <a href="#">域名问题</a>
                    <a href="#">特色功能</a>
                    <a href="#">管理网站</a>
                    <a href="#">新闻咨询</a>
                    <a href="#">使用教程</a>
                </div>
            </div>
        </div>
        <div class="mlist news">
            <div class="module_container">
                <div class="container_content">
                    <div class="content_wrapper">
                        {{include './partial/news_list.tpl'}}
                    </div>
                </div>
                {{include './partial/pages.tpl'}}
            </div>
        </div>
    </div>
</div>
{{include './partial/footer.tpl'}}
